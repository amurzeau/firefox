# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### Strings for the upgrade dialog that can be displayed on major version change.


## New changes screen

## Pin Firefox screen
##
## These title, subtitle and button strings differ between platforms as they
## match the OS' application context menu item action where Windows uses "pin"
## and "taskbar" while macOS "keep" and "Dock" (proper noun).

## Default browser screen

## Theme selection screen

## Start screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-start-title = Libben yn kleur
upgrade-dialog-start-subtitle = Libbene nije kleuren. Beskikber foar in beheinde tiid.
upgrade-dialog-start-primary-button = Kleurstellingen ferkenne.
upgrade-dialog-start-secondary-button = No net

## Colorway screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-colorway-title = Kies jo palet
# This is shown to users with a custom home page, so they can switch to default.
upgrade-dialog-colorway-home-checkbox = Skeakel oer nei Firefox Home mei in tematyske eftergrûn
upgrade-dialog-colorway-primary-button = Kleurstelling bewarje
upgrade-dialog-colorway-secondary-button = Foarige tema behâlde
upgrade-dialog-colorway-theme-tooltip =
    .title = Standerttema’s ferkenne
# $colorwayName (String) - Name of colorway, e.g., Abstract, Cheers
upgrade-dialog-colorway-colorway-tooltip =
    .title = Kleurstellingen { $colorwayName } ûntdekke
upgrade-dialog-colorway-default-theme = Standert
# "Auto" is short for "Automatic"
upgrade-dialog-colorway-theme-auto = Automatysk
    .title = It bestjoeringssysteem folgje foar knoppen, menu’s en finsters
upgrade-dialog-theme-light = Ljocht
    .title = In ljocht tema foar knoppen, menu’s en finsters brûke
upgrade-dialog-theme-dark = Donker
    .title = In donker tema foar knoppen, menu’s en finsters brûke
upgrade-dialog-colorway-variation-soft = Sêft
    .title = Dizze kleurstelling brûke
upgrade-dialog-colorway-variation-balanced = Balansearre
    .title = Dizze kleurstelling brûke
# "Bold" is used in the sense of bravery or courage, not in the sense of
# emphasized text.
upgrade-dialog-colorway-variation-bold = Stevich
    .title = Dizze kleurstelling brûke

## Thank you screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-thankyou-title = Tank dat jo keazen hawwe foar ús
upgrade-dialog-thankyou-subtitle = { -brand-short-name } is in ûnôfhinklike browser dy’t stipe wurdt troch in non-profit. Tegearre meitsje wy it web feiliger, sûner en mear privee.
upgrade-dialog-thankyou-primary-button = Start mei browsen
