# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

toolbar-context-menu-menu-bar =
    .toolbarname = Menubalke
    .accesskey = M

## Tools Menu

menu-tools-settings =
    .label = Ynstellingen
    .accesskey = e
menu-addons-and-themes =
    .label = Add-ons en tema’s
    .accesskey = A

## Help Menu

menu-help-enter-troubleshoot-mode =
    .label = Probleemoplossingsmodus…
    .accesskey = P
menu-help-exit-troubleshoot-mode =
    .label = Probleemoplossingsmodus útskeakelje
    .accesskey = k
menu-help-more-troubleshooting-info =
    .label = Mear probleemoplossingsynformaasje
    .accesskey = M

## Mail Toolbar

toolbar-junk-button =
    .label = Net-winske
    .tooltiptext = De selektearre berjochten as net-winske markearje
toolbar-not-junk-button =
    .label = Net net-winske
    .tooltiptext = De selektearre berjochten as net net-winske markearje
toolbar-delete-button =
    .label = Fuortsmite
    .tooltiptext = Selektearre berjochten of map fuortsmite
toolbar-undelete-button =
    .label = Fuortsmiten ûngedien meitsje
    .tooltiptext = Fuortsmiten fan selektearre berjochten ûngedien meitsje

## View

menu-view-repair-text-encoding =
    .label = Tekstkodearring reparearje
    .accesskey = r

## View / Layout

menu-font-size-label =
    .label = Lettergrutte
    .accesskey = u
menuitem-font-size-enlarge =
    .label = Lettergrutte fergrutsje
    .accesskey = f
menuitem-font-size-reduce =
    .label = Lettergrutte ferlytse
    .accesskey = l
menuitem-font-size-reset =
    .label = Lettergrutte opnij ynstelle
    .accesskey = n
mail-uidensity-label =
    .label = Tichtens
    .accesskey = T
mail-uidensity-compact =
    .label = Kompakt
    .accesskey = K
mail-uidensity-normal =
    .label = Normaal
    .accesskey = N
mail-uidensity-touch =
    .label = Oanraking
    .accesskey = O
menu-spaces-toolbar-button =
    .label = Taakbalke
    .accesskey = T

## File

file-new-newsgroup-account =
    .label = Nijsgroepaccount…
    .accesskey = N
