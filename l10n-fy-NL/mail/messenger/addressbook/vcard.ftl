# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, you can obtain one at http://mozilla.org/MPL/2.0/.


# Display Name

vcard-displayname = Werjeftenamme
vcard-displayname-placeholder =
    .placeholder = { vcard-displayname }

# Type selection

vcard-entry-type-label = Type
vcard-entry-type-home = Thús
vcard-entry-type-work = Wurk
vcard-entry-type-none = Gjin
vcard-entry-type-custom = Oanpast

# N vCard field

vcard-name-header = Namme
vcard-n-prefix = Foarheaksel
vcard-n-add-prefix =
    .title = Foarheaksel tafoegje
vcard-n-firstname = Foarnamme
vcard-n-add-firstname =
    .title = Foarnamme tafoegje
vcard-n-middlename = Twadde namme
vcard-n-add-middlename =
    .title = Twadde namme tafoegje
vcard-n-lastname = Efternamme
vcard-n-add-lastname =
    .title = Efternamme tafoegje
vcard-n-suffix = Efterheaksel
vcard-n-add-suffix =
    .title = Efterheaksel tafoegje

# Email vCard field

vcard-email-header = E-mailadressen
vcard-email-add = E-mailadres tafoegje
vcard-email-label = E-mailadres
vcard-primary-email-label = Standert

# URL vCard field

vcard-url-header = Websites
vcard-url-add = Website tafoegje
vcard-url-label = Website

# Tel vCard field

vcard-tel-header = Telefoannûmers
vcard-tel-add = Telefoannûmer tafoegje
vcard-tel-label = Telefoannûmer

# TZ vCard field

vcard-tz-header = Tiidsône
vcard-tz-add = Tiidsône tafoegje

# IMPP vCard field

vcard-impp-header = Chataccounts
vcard-impp-add = Chataccount tafoegje
vcard-impp-label = Chataccount

# BDAY and ANNIVERSARY vCard field

vcard-bday-anniversary-header = Spesjale data
vcard-bday-anniversary-add = Spesjale datum tafoegje
vcard-bday-label = Jierdei
vcard-anniversary-label = Jubileum
vcard-date-day = Dei
vcard-date-month = Moanne
vcard-date-year = Jier

# ADR vCard field

vcard-adr-header = Adressen
vcard-adr-add = Adres tafoegje
vcard-adr-label = Adres
vcard-adr-delivery-label = Besoargingslabel
vcard-adr-pobox = Brievebus
vcard-adr-ext = Wiidweidich adres
vcard-adr-street = Adres
# Or "Locality"
vcard-adr-locality = Stêd
# Or "Region"
vcard-adr-region = Steat/provinsje
# The term "ZIP code" only applies in USA. Most locales should use "Postal code" only.
vcard-adr-code = Postkoade
vcard-adr-country = Lân

# NOTE vCard field

vcard-note-header = Opmerkingen
vcard-note-add = Opmerking tafoegje

# TITLE, ROLE and ORGANIZATION vCard fields

vcard-org-header = Organisaasjedetails
vcard-org-add = Organisaasjedetails tafoegje
vcard-org-title = Titel
vcard-org-role = Rol
vcard-org-org = Organisaasje
