# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### Strings for the upgrade dialog that can be displayed on major version change.


## New changes screen

## Pin Firefox screen
##
## These title, subtitle and button strings differ between platforms as they
## match the OS' application context menu item action where Windows uses "pin"
## and "taskbar" while macOS "keep" and "Dock" (proper noun).

## Default browser screen

## Theme selection screen

## Start screen


## Colorway screen

upgrade-dialog-theme-light = Светла
    .title = Користите светлу тему за дугмад, меније и прозоре
upgrade-dialog-theme-dark = Тамна
    .title = Користите тамну тему за дугмад, меније и прозоре

## Thank you screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-thankyou-title = Хвала вам што сте нас изабрали
upgrade-dialog-thankyou-subtitle = { -brand-short-name } је независтан прегледач подржан од стране непрофитног удружења. Заједно стварамо безбеднији, здравији и приватнији веб.
upgrade-dialog-thankyou-primary-button = Започни прегледање
