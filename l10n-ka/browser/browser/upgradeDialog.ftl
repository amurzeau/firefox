# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### Strings for the upgrade dialog that can be displayed on major version change.


## New changes screen

## Pin Firefox screen
##
## These title, subtitle and button strings differ between platforms as they
## match the OS' application context menu item action where Windows uses "pin"
## and "taskbar" while macOS "keep" and "Dock" (proper noun).

## Default browser screen

## Theme selection screen

## Start screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-start-title = სიცოცხლე ფერებში
upgrade-dialog-start-subtitle = ხასხასა ფერთა შეხამება. ხელმისაწვდომია, მცირე დროით
upgrade-dialog-start-primary-button = მოსინჯეთ შეფერილობა
upgrade-dialog-start-secondary-button = ახლა არა

## Colorway screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-colorway-title = შეარჩიეთ ფერთა შეხამება
# This is shown to users with a custom home page, so they can switch to default.
upgrade-dialog-colorway-home-checkbox = ჩანაცვლდეს Firefox-ის საწყისი გვერდიც
upgrade-dialog-colorway-primary-button = შეფერილობის შენახვა
upgrade-dialog-colorway-secondary-button = დარჩეს წინა გაფორმება
upgrade-dialog-colorway-theme-tooltip =
    .title = მოსინჯეთ ნაგულისხმევი შეფერილობა
# $colorwayName (String) - Name of colorway, e.g., Abstract, Cheers
upgrade-dialog-colorway-colorway-tooltip =
    .title = მოსინჯეთ { $colorwayName } შეფერილობა
upgrade-dialog-colorway-default-theme = ნაგულისხმევი
# "Auto" is short for "Automatic"
upgrade-dialog-colorway-theme-auto = თვითშერჩევა
    .title = შეუხამებს სისტემის გაფორმებას, ღილაკებს, მენიუსა და ფანჯრებს
upgrade-dialog-theme-light = ნათელი
    .title = ნათელი გაფორმება ღილაკებზე, მენიუსა და ფანჯრებზე
upgrade-dialog-theme-dark = მუქი
    .title = მუქი გაფორმება ღილაკებზე, მენიუსა და ფანჯრებზე
upgrade-dialog-colorway-variation-soft = ღია
    .title = ამ შეფერილობის გამოყენება
upgrade-dialog-colorway-variation-balanced = საშუალო
    .title = ამ შეფერილობის გამოყენება
# "Bold" is used in the sense of bravery or courage, not in the sense of
# emphasized text.
upgrade-dialog-colorway-variation-bold = მუქი
    .title = ამ შეფერილობის გამოყენება

## Thank you screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-thankyou-title = გმადლობთ, რომ ჩვენ აგვირჩიეთ
upgrade-dialog-thankyou-subtitle = { -brand-short-name } დამოუკიდებელი ბრაუზერია, არამომგებიანი დაწესებულებისგან. ერთად, ჩვენ ვქმნით მეტად უსაფრთხო, ჯანსაღ და პირადულ ვებსამყაროს.
upgrade-dialog-thankyou-primary-button = გვერდების მონახულება
