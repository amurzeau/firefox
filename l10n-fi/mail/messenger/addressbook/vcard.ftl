# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, you can obtain one at http://mozilla.org/MPL/2.0/.


# Display Name

vcard-displayname = Näyttönimi
vcard-displayname-placeholder =
    .placeholder = { vcard-displayname }

# Type selection

vcard-entry-type-label = Tyyppi
vcard-entry-type-home = Koti
vcard-entry-type-work = Työ
vcard-entry-type-none = Ei mitään
vcard-entry-type-custom = Mukautettu

# N vCard field

vcard-name-header = Nimi
vcard-n-prefix = Etuliite
vcard-n-add-prefix =
    .title = Lisää etuliite
vcard-n-firstname = Etunimi
vcard-n-add-firstname =
    .title = Lisää etunimi
vcard-n-middlename = Toinen nimi
vcard-n-add-middlename =
    .title = Lisää toinen nimi
vcard-n-lastname = Sukunimi
vcard-n-add-lastname =
    .title = Lisää sukunimi
vcard-n-suffix = Jälkiliite
vcard-n-add-suffix =
    .title = Lisää jälkiliite

# Email vCard field

vcard-email-header = Sähköpostiosoitteet
vcard-email-add = Lisää sähköpostiosoite
vcard-email-label = Sähköpostiosoite
vcard-primary-email-label = Oletus

# URL vCard field

vcard-url-header = Verkkosivustot
vcard-url-add = Lisää verkkosivusto
vcard-url-label = Verkkosivusto

# Tel vCard field

vcard-tel-header = Puhelinnumerot
vcard-tel-add = Lisää puhelinnumero
vcard-tel-label = Puhelinnumero

# TZ vCard field

vcard-tz-header = Aikavyöhyke
vcard-tz-add = Lisää aikavyöhyke

# IMPP vCard field

vcard-impp-header = Pikaviestitilit
vcard-impp-add = Lisää pikaviestitili
vcard-impp-label = Pikaviestitili

# BDAY and ANNIVERSARY vCard field


# ADR vCard field

vcard-adr-header = Osoitteet
vcard-adr-add = Lisää osoite
vcard-adr-label = Osoite
vcard-adr-street = Katuosoite
# Or "Locality"
vcard-adr-locality = Kaupunki

# NOTE vCard field


# TITLE, ROLE and ORGANIZATION vCard fields

vcard-org-role = Rooli
vcard-org-org = Organisaatio
