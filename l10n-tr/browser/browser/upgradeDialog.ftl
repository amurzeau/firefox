# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### Strings for the upgrade dialog that can be displayed on major version change.


## New changes screen

## Pin Firefox screen
##
## These title, subtitle and button strings differ between platforms as they
## match the OS' application context menu item action where Windows uses "pin"
## and "taskbar" while macOS "keep" and "Dock" (proper noun).

## Default browser screen

## Theme selection screen

## Start screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-start-title = Hayatınıza renk katın
upgrade-dialog-start-subtitle = Yepyeni renk kuşakları. Sadece sınırlı bir süre için.
upgrade-dialog-start-primary-button = Renk kuşaklarını keşfet
upgrade-dialog-start-secondary-button = Daha sonra

## Colorway screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-colorway-title = Paletinizi seçin
upgrade-dialog-colorway-home-checkbox = Temalı arka plana sahip Firefox giriş sayfasına geç
upgrade-dialog-colorway-primary-button = Renk kuşağını kaydet
upgrade-dialog-colorway-secondary-button = Önceki temayı kullanmaya devam et
upgrade-dialog-colorway-theme-tooltip =
    .title = Varsayılan temaları keşfet
# $colorwayName (String) - Name of colorway, e.g., Abstract, Cheers
upgrade-dialog-colorway-colorway-tooltip =
    .title = { $colorwayName } renk kuşaklarını keşfedin
upgrade-dialog-colorway-default-theme = Varsayılan
# "Auto" is short for "Automatic"
upgrade-dialog-colorway-theme-auto = Otomatik
    .title = Düğmeler, menüler ve pencereler için işletim sisteminin temasını kullan
upgrade-dialog-theme-light = Açık
    .title = Düğmeler, menüler ve pencereler için açık bir tema kullan
upgrade-dialog-theme-dark = Koyu
    .title = Düğmeler, menüler ve pencereler için koyu bir tema kullan
upgrade-dialog-colorway-variation-soft = Yumuşak
    .title = Bu renk kuşağını kullan
upgrade-dialog-colorway-variation-balanced = Dengeli
    .title = Bu renk kuşağını kullan
# "Bold" is used in the sense of bravery or courage, not in the sense of
# emphasized text.
upgrade-dialog-colorway-variation-bold = Canlı
    .title = Bu renk kuşağını kullan

## Thank you screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-thankyou-title = Bizi tercih ettiğiniz için teşekkürler
upgrade-dialog-thankyou-subtitle = { -brand-short-name }, kâr amacı gütmeyen bir kuruluşun elinden çıkan bağımsız bir tarayıcıdır. Birlikte web’i daha güvenli, sağlıklı ve gizlilik yanlısı bir hale getiriyoruz.
upgrade-dialog-thankyou-primary-button = Gezinmeye başla
