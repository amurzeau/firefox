# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, you can obtain one at http://mozilla.org/MPL/2.0/.

import-page-title = İçe aktar
export-page-title = Dışa aktar

## Header

import-start = İçe aktarma aracı
import-start-title = Bir uygulamadan veya dosyadan ayarları veya verileri içe aktar.
import-from-app = Uygulamadan içe aktar
import-file = Dosyadan içe aktar
export-profile = Dışa aktar

## Buttons

button-back = Geri
button-continue = Devam et
button-export = Dışa aktar

## Import from app steps

app-name-thunderbird = Thunderbird
app-name-seamonkey = SeaMonkey
app-name-outlook = Outlook
app-name-becky = Becky! Internet Mail
app-name-apple-mail = Apple Mail

## Import from file selections


## Import from app profile steps

items-pane-checkbox-accounts = Hesaplar ve ayarlar
items-pane-checkbox-address-books = Adres defterleri
items-pane-checkbox-calendars = Takvimler
items-pane-checkbox-mail-messages = Posta iletileri

## Import from address book file steps

addr-book-csv-file = Virgülle veya sekmeyle ayrılmış dosya (.csv, .tsv)
addr-book-ldif-file = LDIF dosyası (.ldif)
addr-book-vcard-file = vCard dosyası (.vcf, .vcard)
addr-book-sqlite-file = SQLite veritabanı dosyası (.sqlite)
addr-book-mab-file = Mork veritabanı dosyası (.mab)
addr-book-file-picker = Bir adres defteri dosyası seçin
addr-book-directories-pane-source = Kaynak dosya:

## Import from calendar file steps

calendar-items-loading = Öğeler yükleniyor…
calendar-items-filter-input =
    .placeholder = Öğeleri filtrele…
calendar-select-all-items = Tümünü seç
calendar-deselect-all-items = Seçimi temizle

## Import dialog

error-pane-title = Hata
error-message-failed = İçe aktarma beklenmedik bir şekilde başarısız oldu. Hata Konsolu'nda daha fazla bilgi mevcut olabilir.
error-failed-to-parse-ics-file = Dosyada içe aktarılabilir öğe bulunamadı.
error-export-failed = Dışa aktarma beklenmedik bir şekilde başarısız oldu. Hata Konsolu'nda daha fazla bilgi mevcut olabilir.

## <csv-field-map> element

csv-first-row-contains-headers = İlk satır, alan adlarını içerir
csv-source-field = Kaynak alanı
csv-source-first-record = İlk kayıt
csv-source-second-record = İkinci kayıt
csv-target-field = Adres defteri alanı

## Export tab

export-open-profile-folder = Profil klasörünü aç
export-brand-name = { -brand-product-name }

## Summary pane


## Footer area


## Step navigation on top of the wizard pages

