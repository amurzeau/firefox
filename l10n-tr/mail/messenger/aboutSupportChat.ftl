# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

chat-title = Sohbet hesapları
chat-table-heading-account = Kimlik
chat-table-heading-protocol = Protokol
chat-table-heading-name = Adı
chat-table-heading-actions = Eylemler
