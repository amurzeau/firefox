# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

protections-close-button2 =
    .aria-label = Затвори
    .title = Затвори


lockwise-how-it-works-link = Како работи

monitor-link = Како работи

monitor-no-breaches-title = Добри вести!

monitor-partial-breaches-motivation-title-start = Одличен почеток!
monitor-partial-breaches-motivation-title-middle = Продолжи така!

## The title attribute is used to display the type of protection.
## The aria-label is spoken by screen readers to make the visual graph accessible to blind users.
##
## Variables:
##   $count (Number) - Number of specific trackers
##   $percentage (Number) - Percentage this type of tracker contributes to the whole graph

