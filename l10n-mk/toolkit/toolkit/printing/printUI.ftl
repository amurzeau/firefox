# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

printui-title = Печати
# Dialog title to prompt the user for a filename to save print to PDF.
printui-save-to-pdf-title = Сними како

printui-page-range-all = Сè
printui-page-range-odd = Непарни
printui-page-range-even = Парни
printui-page-range-label = Страници

# Section title for the number of copies to print
printui-copies-label = Копии

printui-orientation = Ориентација
printui-landscape = Пејсаж
printui-portrait = Портрет

# Section title for the printer or destination device to target
printui-destination-label = Дестинација
printui-destination-pdf-label = Зачувај како PDF

printui-more-settings = Повеќе поставки

# Section title (noun) for the print scaling options
printui-scale = Големина

# Section title for miscellaneous print options
printui-options = Опции

## The "Format" section, select a version of the website to print. Radio
## options to select between the original page, selected text only, or a version
## where the page is processed with "Reader View".


##

printui-color-mode-color = Боја
printui-color-mode-bw = Црно и бело

printui-margins = Маргини
printui-margins-min = Минимум
printui-margins-custom-bottom = Дно
printui-margins-custom-left = Лево

printui-primary-button = Печати
printui-primary-button-save = Сними
printui-cancel-button = Откажи
printui-close-button = Затвори

# Reported by screen readers and other accessibility tools to indicate that
# the print preview has focus.
printui-preview-label =
    .aria-label = Преглед за печатење

# This is shown next to the Print button with an indefinite loading spinner
# when the user prints a page and it is being sent to the printer.
printui-print-progress-indicator = Се печати...
printui-print-progress-indicator-saving = Се снима…

## Paper sizes that may be supported by the Save to PDF destination:

printui-paper-a5 = A5
printui-paper-a4 = A4
printui-paper-a3 = A3
printui-paper-a2 = A2
printui-paper-a1 = A1
printui-paper-a0 = A0
printui-paper-b5 = B5
printui-paper-b4 = B4
printui-paper-jis-b5 = JIS-B5
printui-paper-jis-b4 = JIS-B4

## Error messages shown when a user has an invalid input

