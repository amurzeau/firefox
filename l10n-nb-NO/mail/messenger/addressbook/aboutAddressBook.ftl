# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, you can obtain one at http://mozilla.org/MPL/2.0/.

about-addressbook-title = Adressebok

## Toolbar

about-addressbook-toolbar-new-address-book =
    .label = Ny adressebok
about-addressbook-toolbar-new-contact =
    .label = Ny kontakt
about-addressbook-toolbar-new-list =
    .label = Ny liste

## Books

all-address-books = Alle adressebøker

about-addressbook-books-context-properties =
    .label = Egenskaper
about-addressbook-books-context-synchronize =
    .label = Synkroniser
about-addressbook-books-context-delete =
    .label = Slett
about-addressbook-books-context-remove =
    .label = Fjern

about-addressbook-confirm-delete-book-title = Slett adressebok

## Cards


## Details

about-addressbook-begin-edit-contact-button = Rediger
about-addressbook-cancel-edit-contact-button = Avbryt
about-addressbook-save-edit-contact-button = Lagre

# Photo dialog

