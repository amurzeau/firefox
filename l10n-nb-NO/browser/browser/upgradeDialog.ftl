# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### Strings for the upgrade dialog that can be displayed on major version change.


## New changes screen

## Pin Firefox screen
##
## These title, subtitle and button strings differ between platforms as they
## match the OS' application context menu item action where Windows uses "pin"
## and "taskbar" while macOS "keep" and "Dock" (proper noun).

## Default browser screen

## Theme selection screen

## Start screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-start-title = Livet i farger
upgrade-dialog-start-subtitle = Levende nye fargesammensettinger. Tilgjengelig i en begrenset periode.
upgrade-dialog-start-primary-button = Utforsk fargesammensettinger
upgrade-dialog-start-secondary-button = Ikke nå

## Colorway screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-colorway-title = Velg din palett
upgrade-dialog-colorway-home-checkbox = Bytt til Firefox-startside med temabakgrunn
upgrade-dialog-colorway-primary-button = Lagre fargesammensetting
upgrade-dialog-colorway-secondary-button = Behold forrige tema
upgrade-dialog-colorway-theme-tooltip =
    .title = Utforsk standardtemaer
# $colorwayName (String) - Name of colorway, e.g., Abstract, Cheers
upgrade-dialog-colorway-colorway-tooltip =
    .title = Utforsk { $colorwayName }-fargesammensettinger
upgrade-dialog-colorway-default-theme = Standard
# "Auto" is short for "Automatic"
upgrade-dialog-colorway-theme-auto = Automatisk
    .title = Følg operativsystemets tema for knapper, menyer og vinduer
upgrade-dialog-theme-light = Lyst
    .title = Bruk et lyst tema for knapper, menyer og vinduer
upgrade-dialog-theme-dark = Mørkt
    .title = Bruk et mørkt tema for knapper, menyer og vinduer
upgrade-dialog-colorway-variation-soft = Myk
    .title = Bruk denne fargesammensettingen
upgrade-dialog-colorway-variation-balanced = Balansert
    .title = Bruk denne fargesammensettingen
# "Bold" is used in the sense of bravery or courage, not in the sense of
# emphasized text.
upgrade-dialog-colorway-variation-bold = Modig
    .title =
        Bruk dennne fargesammensettingen
        Bruk denne fargesammensetningen

## Thank you screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-thankyou-title = Takk for at du valgte oss
upgrade-dialog-thankyou-subtitle = { -brand-short-name } er en uavhengig nettleser som støttes av en ideell organisasjon. Sammen gjør vi nettet tryggere, sunnere og mer privat.
upgrade-dialog-thankyou-primary-button = Begynn å surfe
