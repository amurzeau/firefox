# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### Strings for the upgrade dialog that can be displayed on major version change.


## New changes screen

## Pin Firefox screen
##
## These title, subtitle and button strings differ between platforms as they
## match the OS' application context menu item action where Windows uses "pin"
## and "taskbar" while macOS "keep" and "Dock" (proper noun).

## Default browser screen

## Theme selection screen

## Start screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-start-title = ਜ਼ਿੰਦਗੀ ਵਿੱਚ ਰੰਗ
upgrade-dialog-start-subtitle = ਵੱਖੋ-ਵੱਖ ਨਵੇਂ ਰੰਗ। ਹੁਣ ਸੀਮਿਤ ਸਮੇਂ ਲਈ ਹੀ ਮੌਜੂਦ ਹਨ।
upgrade-dialog-start-primary-button = ਰੰਗ-ਢੰਗਾਂ ਬਾਰੇ ਜਾਣਕਾਰੀ ਲਵੋ
upgrade-dialog-start-secondary-button = ਹੁਣੇ ਨਹੀਂ

## Colorway screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-colorway-title = ਆਪਣਾ ਰੰਗ-ਸਮੂਹ ਚੁਣੋ
# This is shown to users with a custom home page, so they can switch to default.
upgrade-dialog-colorway-home-checkbox = ਵਿਸ਼ੇ ਵਾਲੀ ਬੈਕਗਰਾਊਂਡ ਨਾਲ Firefox ਮੁੱਖ-ਸਫ਼ੇ ਲਈ ਬਦਲੋ
upgrade-dialog-colorway-primary-button = ਰੰਗ-ਢੰਗ ਸੰਭਾਲੋ
upgrade-dialog-colorway-secondary-button = ਪਿਛਲਾ ਥੀਮ ਲਿਆਓ
upgrade-dialog-colorway-theme-tooltip =
    .title = ਮੂਲ ਥੀਮਾਂ ਬਾਰੇ ਜਾਣਕਾਰੀ ਲਵੋ
# $colorwayName (String) - Name of colorway, e.g., Abstract, Cheers
upgrade-dialog-colorway-colorway-tooltip =
    .title = { $colorwayName } ਰੰਗਾਂ-ਢੰਗਾਂ ਬਾਰੇ ਜਾਣਕਾਰੀ ਲਵੋ
upgrade-dialog-colorway-default-theme = ਮੂਲ
# "Auto" is short for "Automatic"
upgrade-dialog-colorway-theme-auto = ਆਪੇ
    .title = ਬਟਨਾਂ, ਮੇਨੂ ਤੇ ਵਿੰਡੋਆਂ ਲਈ ਓਪਰੇਟਿੰਗ ਸਿਸਟਮ ਥੀਮ ਦੀ ਪਾਲਣਾ ਕਰੋ
upgrade-dialog-theme-light = ਹਲਕਾ
    .title = ਬਟਨਾਂ, ਮੇਨੂ ਤੇ ਵਿੰਡੋਈਆਂ ਲਈ ਹਲਕਾ ਥੀਮ ਵਰਤੋ
upgrade-dialog-theme-dark = ਗੂੜ੍ਹਾ
    .title = ਬਟਨਾਂ, ਮੇਨੂ ਤੇ ਵਿੰਡੋਈਆਂ ਲਈ ਗੂੜ੍ਹਾ ਥੀਮ ਵਰਤੋ
upgrade-dialog-colorway-variation-soft = ਹਲਕਾ
    .title = ਇਹ ਰੰਗ-ਢੰਗ ਵਰਤੋਂ
upgrade-dialog-colorway-variation-balanced = ਸੰਤੁਲਿਤ
    .title = ਇਹ ਰੰਗ-ਢੰਗ ਵਰਤੋਂ
# "Bold" is used in the sense of bravery or courage, not in the sense of
# emphasized text.
upgrade-dialog-colorway-variation-bold = ਗੂੜ੍ਹਾ
    .title = ਇਹ ਰੰਗ-ਢੰਗ ਵਰਤੋਂ

## Thank you screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-thankyou-title = ਸਾਨੂੰ ਚੁਣਨ ਲਈ ਤੁਹਾਡਾ ਧੰਨਵਾਦ ਹੈ
upgrade-dialog-thankyou-subtitle = { -brand-short-name } ਗ਼ੈਰ-ਫਾਇਦਾ ਸੰਗਠਨ ਵਲੋਂ ਤਿਆਰ ਕੀਤਾ ਆਜ਼ਾਦ ਬਰਾਊਜ਼ਰ ਹੈ। ਮਿਲ ਕੇ ਅਸੀਂ ਵੈੱਬ ਨੂੰ ਵੱਧ ਸੁਰੱਖਿਅਤ, ਮਜ਼ਬੂਤ ਅਤੇ ਵੱਧ ਨਿੱਜੀ ਬਣਾ ਰਹੇ ਹਾਂ।
upgrade-dialog-thankyou-primary-button = ਬਰਾਊਜ਼ ਕਰੋ
