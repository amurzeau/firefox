# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### Strings for the upgrade dialog that can be displayed on major version change.


## New changes screen

## Pin Firefox screen
##
## These title, subtitle and button strings differ between platforms as they
## match the OS' application context menu item action where Windows uses "pin"
## and "taskbar" while macOS "keep" and "Dock" (proper noun).

## Default browser screen

## Theme selection screen

## Start screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-start-title = Život vo farbách
upgrade-dialog-start-subtitle = Nové živé farebné prevedenie. K dispozícii na obmedzený čas.
upgrade-dialog-start-primary-button = Preskúmajte farebné témy
upgrade-dialog-start-secondary-button = Teraz nie

## Colorway screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-colorway-title = Zvoľte si paletu
upgrade-dialog-colorway-home-checkbox = Prepnúť na domovskú stránku Firefoxu s témou na pozadí
upgrade-dialog-colorway-primary-button = Uložiť farebnú tému
upgrade-dialog-colorway-secondary-button = Ponechať predchádzajúcu tému
upgrade-dialog-colorway-theme-tooltip =
    .title = Pozrite sa predvolené témy vzhľadu
# $colorwayName (String) - Name of colorway, e.g., Abstract, Cheers
upgrade-dialog-colorway-colorway-tooltip =
    .title = Pozrite sa na farebnú tému { $colorwayName }
upgrade-dialog-colorway-default-theme = Predvolená
# "Auto" is short for "Automatic"
upgrade-dialog-colorway-theme-auto = Automatická
    .title = Nasleduje nastavenia operačného systému pre tlačidlá, ponuky a okná.
upgrade-dialog-theme-light = Svetlá
    .title = Použije svetlú tému pre tlačidlá, ponuky a okná
upgrade-dialog-theme-dark = Tmavá
    .title = Použije tmavú tému pre tlačidlá, ponuky a okná
upgrade-dialog-colorway-variation-soft = Jemná
    .title = Použiť túto farebnú tému
upgrade-dialog-colorway-variation-balanced = Vyvážená
    .title = Použiť túto farebnú tému
# "Bold" is used in the sense of bravery or courage, not in the sense of
# emphasized text.
upgrade-dialog-colorway-variation-bold = Výrazná
    .title = Použiť túto farebnú tému

## Thank you screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-thankyou-title = Ďakujeme, že ste si nás vybrali
upgrade-dialog-thankyou-subtitle = { -brand-short-name } je nezávislý prehliadač podporovaný neziskovou organizáciou. Spoločne robíme web bezpečnejším, zdravším a s väčším ohľadom na súkromie.
upgrade-dialog-thankyou-primary-button = Začať prehliadanie
