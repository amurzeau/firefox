# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## View / Layout

appmenu-font-size-value = Pismowe wjelikosć
appmenuitem-font-size-enlarge =
    .tooltiptext = Pismowu wjelikosć pówětšyś
appmenuitem-font-size-reduce =
    .tooltiptext = Pismowu wjelikosć pómjeńšyś
# Variables:
# $size (String) - The current font size.
appmenuitem-font-size-reset =
    .label = { $size } px
    .tooltiptext = Pismowu wjelikosć slěd stajiś
