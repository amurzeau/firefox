# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### Strings for the upgrade dialog that can be displayed on major version change.


## New changes screen

## Pin Firefox screen
##
## These title, subtitle and button strings differ between platforms as they
## match the OS' application context menu item action where Windows uses "pin"
## and "taskbar" while macOS "keep" and "Dock" (proper noun).

## Default browser screen

## Theme selection screen

## Start screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-start-title = Žywjenje w barwje
upgrade-dialog-start-subtitle = Žywe nowe barwowe kombinacije. Za wobgranicowany cas k dispoziciji.
upgrade-dialog-start-primary-button = Barwowe kombinacije wuslěźiś
upgrade-dialog-start-secondary-button = Nic něnto

## Colorway screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-colorway-title = Wubjeŕśo swóju paletu
upgrade-dialog-colorway-home-checkbox = K startowemu bokoju Firefox z drastwoweju slězynu pśejś
upgrade-dialog-colorway-primary-button = Barwowu kombinaciju składowaś
upgrade-dialog-colorway-secondary-button = Pjerwjejšnu drastww wobchowaś
upgrade-dialog-colorway-theme-tooltip =
    .title = Standardne drastwy wuslěźiś
# $colorwayName (String) - Name of colorway, e.g., Abstract, Cheers
upgrade-dialog-colorway-colorway-tooltip =
    .title = Wuslěźćo barwowe kombinacije { $colorwayName }
upgrade-dialog-colorway-default-theme = Standard
# "Auto" is short for "Automatic"
upgrade-dialog-colorway-theme-auto = Awtomatiski
    .title = Drastwu źěłowego systema za tłocaški, menije a wokna wužywaś
upgrade-dialog-theme-light = Swětły
    .title = Swětłu drastwu za tłocaški, menije a wokna wužywaś
upgrade-dialog-theme-dark = Śamny
    .title = Śamnu drastwu za tłocaški, menije a wokna wužywaś
upgrade-dialog-colorway-variation-soft = Mělny
    .title = Toś tu barwowu kombinaciju wužywaś
upgrade-dialog-colorway-variation-balanced = Wurownany
    .title = Toś tu barwowu kombinaciju wužywaś
# "Bold" is used in the sense of bravery or courage, not in the sense of
# emphasized text.
upgrade-dialog-colorway-variation-bold = Intensiwny
    .title = Toś tu barwowu kombinaciju wužywaś

## Thank you screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-thankyou-title = Źěkujomy se, až sćo nas wubrał
upgrade-dialog-thankyou-subtitle = { -brand-short-name } jo njewótwisny wobhlědowak za wše wužytneje organizacije. Gromaźe cynimy web wěsćejšy, strowšy a priwatnjejšy.
upgrade-dialog-thankyou-primary-button = Pśeglědowanje zachopiś
