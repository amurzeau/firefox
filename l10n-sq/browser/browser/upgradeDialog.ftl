# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### Strings for the upgrade dialog that can be displayed on major version change.


## New changes screen

## Pin Firefox screen
##
## These title, subtitle and button strings differ between platforms as they
## match the OS' application context menu item action where Windows uses "pin"
## and "taskbar" while macOS "keep" and "Dock" (proper noun).

## Default browser screen

## Theme selection screen

## Start screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-start-title = Jetë me ngjyra
upgrade-dialog-start-subtitle = Kombinime të reja drithëruese ngjyrash. Të passhme brenda një kohe të kufizuar.
upgrade-dialog-start-primary-button = Eksploroni kombinime ngjyrash
upgrade-dialog-start-secondary-button = Jo tani

## Colorway screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-colorway-title = Zgjidhni paletën tuaj
# This is shown to users with a custom home page, so they can switch to default.
upgrade-dialog-colorway-home-checkbox = Kalo në Kreun e Firefox-it me sfond të përshtatur
upgrade-dialog-colorway-primary-button = Ruaje kombinimin e ngjyrave
upgrade-dialog-colorway-secondary-button = Mbaj temën e mëparshme
upgrade-dialog-colorway-theme-tooltip =
    .title = Eksploroni tema parazgjedhje
# $colorwayName (String) - Name of colorway, e.g., Abstract, Cheers
upgrade-dialog-colorway-colorway-tooltip =
    .title = Eksploroni kombinime ngjyrash { $colorwayName }
upgrade-dialog-colorway-default-theme = Parazgjedhje
# "Auto" is short for "Automatic"
upgrade-dialog-colorway-theme-auto = Auto
    .title = Për butona, menu dhe dritare, ndiq temën e sistemit operativ
upgrade-dialog-theme-light = E çelët
    .title = Për butona, menu dhe dritare përdor një temë të çelët
upgrade-dialog-theme-dark = E errët
    .title = Për butona, menu dhe dritare përdor një temë të errët
upgrade-dialog-colorway-variation-soft = I butë
    .title = Përdor këtë kombinim ngjyrash
upgrade-dialog-colorway-variation-balanced = I baraspeshuar
    .title = Përdor këtë kombinim ngjyrash
# "Bold" is used in the sense of bravery or courage, not in the sense of
# emphasized text.
upgrade-dialog-colorway-variation-bold = I guximshëm
    .title = Përdor këtë kombinim ngjyrash

## Thank you screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-thankyou-title = Faleminderit që na zgjidhni ne
upgrade-dialog-thankyou-subtitle = { -brand-short-name }-i është një shfletues i pavarur, me në ent jofitimprurës nga pas. Tok, po e bëmë web-in më të sigurt, më të shëndetshëm dhe më privat.
upgrade-dialog-thankyou-primary-button = Nisni shfletimin
