# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### UI strings for the simplified onboarding / multistage about:welcome
### Various strings use a non-breaking space to avoid a single dangling /
### widowed word, so test on various window sizes if you also want this.


### UI strings for the MR1 onboarding / multistage about:welcome
### Various strings use a non-breaking space to avoid a single dangling /
### widowed word, so test on various window sizes if you also want this.


## Welcome page strings

onboarding-welcome-header = Bine ai venit la { -brand-short-name }
onboarding-start-browsing-button-label = Începe să navighezi
onboarding-not-now-button-label = Nu acum

## Custom Return To AMO onboarding strings


## Multistage 3-screen onboarding flow strings (about:welcome pages)

## Multistage onboarding strings (about:welcome pages)

# Aria-label to make the "steps" of multistage onboarding visible to screen readers.
# Variables:
#   $current (Int) - Number of the current page
#   $total (Int) - Total number of pages
onboarding-welcome-steps-indicator =
    .aria-label = Pentru început: ecran { $current } din { $total }

# This button will open system settings to turn on prefers-reduced-motion
mr1-onboarding-reduce-motion-button-label = Dezactivează animațiile

## Title and primary button strings differ between platforms as they
## match the OS' application context menu item action where Windows uses "pin"
## and "taskbar" while macOS "keep" and "Dock" (proper noun).

# Title used on welcome page when Firefox is not pinned
mr1-onboarding-pin-header =
    { PLATFORM() ->
        [macos] Păstrează { -brand-short-name } în Dock pentru acces facil
       *[other] Fixează { -brand-short-name } în bara de activități pentru acces facil
    }
# Primary button string used on welcome page when Firefox is not pinned.
mr1-onboarding-pin-primary-button-label =
    { PLATFORM() ->
        [macos] Păstrează în Dock
       *[other] Fixează în bara de activități
    }

## Multistage MR1 onboarding strings (about:welcome pages)

mr1-onboarding-set-default-pin-primary-button-label = Desemnează { -brand-short-name } ca browserul meu principal
    .title = Setează { -brand-short-name } drept browser implict și fixează în bara de activități

# This string will be used on welcome page primary button label
# when Firefox is not default but already pinned
mr1-onboarding-set-default-only-primary-button-label = Desemnează { -brand-short-name } ca browserul meu implicit
mr1-onboarding-set-default-secondary-button-label = Nu acum

## Title, subtitle and primary button string used on set default onboarding screen
## when Firefox is not default browser

mr1-onboarding-default-header = Desemnează { -brand-short-name } ca browser implicit
mr1-onboarding-default-subtitle = Pune viteza, siguranța și confidențialitatea pe pilot automat.
mr1-onboarding-default-primary-button-label = Desemnează ca browser implicit

## Multistage MR1 onboarding strings (about:welcome pages)

mr1-onboarding-import-secondary-button-label = Nu acum

mr1-onboarding-theme-subtitle = Personalizează { -brand-short-name } cu o temă.
mr1-onboarding-theme-primary-button-label = Salvează tema
mr1-onboarding-theme-secondary-button-label = Nu acum

# System theme uses operating system color settings
mr1-onboarding-theme-label-system = Tema sistemului

# "Alpenglow" here is the name of the theme, and should be kept in English.
mr1-onboarding-theme-label-alpenglow = Alpenglow

## Please make sure to split the content of the title attribute into lines whose
## width corresponds to about 40 Latin characters, to ensure that the tooltip
## doesn't become too long. Line breaks will be preserved when displaying the
## tooltip.


## Please make sure to split the content of the title attribute into lines whose
## width corresponds to about 40 Latin characters, to ensure that the tooltip
## doesn't become too long. Line breaks will be preserved when displaying the
## tooltip.

## Multistage MR1 onboarding strings (MR1 about:welcome pages)

# Tooltip displayed on hover of system theme
mr1-onboarding-theme-tooltip-system =
    .title =
        Urmează tema sistemului de operare
        pentru butoane, meniuri și ferestre.

# Input description for system theme
mr1-onboarding-theme-description-system =
    .aria-description =
        Urmează tema sistemului de operare
        pentru butoane, meniuri și ferestre.

## Strings for Thank You page

## Multistage live language reloading onboarding strings (about:welcome pages)
##
## The following language names are generated by the browser's Intl.DisplayNames API.
##
## Variables:
##   $negotiatedLanguage (String) - The name of the langpack's language, e.g. "Español (ES)"

## Firefox 100 Thank You screens

