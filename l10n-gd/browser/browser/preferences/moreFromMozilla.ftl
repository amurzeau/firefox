# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## These strings are used in the about:preferences moreFromMozilla page

more-from-moz-title = Barrachd o { -vendor-short-name }
more-from-moz-category =
    .tooltiptext = Barrachd o { -vendor-short-name }
more-from-moz-button-mozilla-vpn-2 = Faigh VPN
more-from-moz-learn-more-link = Barrachd fiosrachaidh
