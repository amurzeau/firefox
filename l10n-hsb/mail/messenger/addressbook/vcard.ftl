# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, you can obtain one at http://mozilla.org/MPL/2.0/.


# Display Name

vcard-displayname = Zwobraznjenske mjeno
vcard-displayname-placeholder =
    .placeholder = { vcard-displayname }

# Type selection

vcard-entry-type-label = Typ
vcard-entry-type-home = Startowa strona
vcard-entry-type-work = Słužbny
vcard-entry-type-none = Žadyn
vcard-entry-type-custom = Swójski

# N vCard field

vcard-name-header = Mjeno
vcard-n-prefix = Prefiks
vcard-n-add-prefix =
    .title = Prefiks přidać
vcard-n-firstname = Předmjeno
vcard-n-add-firstname =
    .title = Předmjeno přidać
vcard-n-middlename = Druhe předmjeno
vcard-n-add-middlename =
    .title = Druhe předmjeno přidać
vcard-n-lastname = Swójbne mjeno
vcard-n-add-lastname =
    .title = Swójbne mjeno přidać
vcard-n-suffix = Sufiks
vcard-n-add-suffix =
    .title = Sufiks přidać

# Email vCard field

vcard-email-header = E-mejlowe adresy
vcard-email-add = E-mejlowu adresu přidać
vcard-email-label = E-mejlowa adresa
vcard-primary-email-label = Standard

# URL vCard field

vcard-url-header = Websydła
vcard-url-add = Websydło přidać
vcard-url-label = Websydło

# Tel vCard field

vcard-tel-header = Telefonowe čisła
vcard-tel-add = Telefonowe čisło přidać
vcard-tel-label = Telefonowe čisło

# TZ vCard field

vcard-tz-header = Časowe pasmo
vcard-tz-add = Časowe pasmo přidać

# IMPP vCard field

vcard-impp-header = Chatowe konta
vcard-impp-add = Chatowe konto přidać
vcard-impp-label = Chatowe konto

# BDAY and ANNIVERSARY vCard field

vcard-bday-anniversary-header = Specialne datumy
vcard-bday-anniversary-add = Specialny datum přidać
vcard-bday-label = Narodniny
vcard-anniversary-label = Róčnica
vcard-date-day = Dźeń
vcard-date-month = Měsac
vcard-date-year = Lěto

# ADR vCard field

vcard-adr-header = Adresy
vcard-adr-add = Adresu přidać
vcard-adr-label = Adresa
vcard-adr-delivery-label = Dodawanski etiket
vcard-adr-pobox = Póstowy fach
vcard-adr-ext = Rozšěrjena adresa
vcard-adr-street = Dróhowa adresa
# Or "Locality"
vcard-adr-locality = Město
# Or "Region"
vcard-adr-region = Zwjazkowy kraj
# The term "ZIP code" only applies in USA. Most locales should use "Postal code" only.
vcard-adr-code = Póstowe wodźenske čisło
vcard-adr-country = Kraj

# NOTE vCard field

vcard-note-header = Přispomnjenki
vcard-note-add = Přispomnjenku přidać

# TITLE, ROLE and ORGANIZATION vCard fields

vcard-org-header = Organizaciske kajkosće
vcard-org-add = Organizaciske kajkosće přidać
vcard-org-title = Titul
vcard-org-role = Róla
vcard-org-org = Organizacija
