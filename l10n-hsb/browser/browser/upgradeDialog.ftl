# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### Strings for the upgrade dialog that can be displayed on major version change.


## New changes screen

## Pin Firefox screen
##
## These title, subtitle and button strings differ between platforms as they
## match the OS' application context menu item action where Windows uses "pin"
## and "taskbar" while macOS "keep" and "Dock" (proper noun).

## Default browser screen

## Theme selection screen

## Start screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-start-title = Žiwjenje w barbje
upgrade-dialog-start-subtitle = Žiwe nowe barbowe kombinacije. Za wobmjezowany čas k dispoziciji.
upgrade-dialog-start-primary-button = Barbowe kombinacije wuslědźić
upgrade-dialog-start-secondary-button = Nic nětko

## Colorway screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-colorway-title = Wubjerće swoju paletu
upgrade-dialog-colorway-home-checkbox = K startowej stronje Firefox z drastowym pozadkom přeńć
upgrade-dialog-colorway-primary-button = Barbowu kombinaciju składować
upgrade-dialog-colorway-secondary-button = Předchadnu drastu wobchować
upgrade-dialog-colorway-theme-tooltip =
    .title = Standardne drasty wuslědźić
# $colorwayName (String) - Name of colorway, e.g., Abstract, Cheers
upgrade-dialog-colorway-colorway-tooltip =
    .title = Wuslědźće barbowe kombinacije { $colorwayName }
upgrade-dialog-colorway-default-theme = Standard
# "Auto" is short for "Automatic"
upgrade-dialog-colorway-theme-auto = Awtomatisce
    .title = Drastu dźěłoweho systema za tłóčatka, menije a wokna wužiwać
upgrade-dialog-theme-light = Swětły
    .title = Swětłu drastu za tłóčatka, menije a wokna wužiwać
upgrade-dialog-theme-dark = Ćmowy
    .title = Ćmowu drastu za tłóčatka, menije a wokna wužiwać
upgrade-dialog-colorway-variation-soft = Cuni
    .title = Tutu barbowu kombinaciju wužiwać
upgrade-dialog-colorway-variation-balanced = Wurunany
    .title = Tutu barbowu kombinaciju wužiwać
# "Bold" is used in the sense of bravery or courage, not in the sense of
# emphasized text.
upgrade-dialog-colorway-variation-bold = Jaskrawy
    .title = Tutu barbowu kombinaciju wužiwać

## Thank you screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-thankyou-title = Dźakujemy so, zo sće nas wubrał
upgrade-dialog-thankyou-subtitle = { -brand-short-name } je njewotwisny wobhladowak powšitkownosći wužitneje organizacije. Hromadźe činimy web wěsćiši, strowši a priwatniši.
upgrade-dialog-thankyou-primary-button = Přehladowanje započeć
