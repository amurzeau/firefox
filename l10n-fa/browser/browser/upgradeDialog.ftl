# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### Strings for the upgrade dialog that can be displayed on major version change.


## New changes screen

## Pin Firefox screen
##
## These title, subtitle and button strings differ between platforms as they
## match the OS' application context menu item action where Windows uses "pin"
## and "taskbar" while macOS "keep" and "Dock" (proper noun).

## Default browser screen

## Theme selection screen

## Start screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-start-title = زندگی با رنگ‌ها
upgrade-dialog-start-subtitle = رنگ‌بندی‌هایِ جدید و با طراوت. در دسترس برای مدتی محدود.
upgrade-dialog-start-primary-button = بررسیِ رنگ‌بندی‌ها
upgrade-dialog-start-secondary-button = اکنون نه

## Colorway screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-colorway-title = پالت خود را انتخاب کنید
# This is shown to users with a custom home page, so they can switch to default.
upgrade-dialog-colorway-home-checkbox = تعویض به خانهٔ فایرفاکس با پیش‌زمینهٔ تم
upgrade-dialog-colorway-primary-button = ذخیره رنگ‌بندی
upgrade-dialog-colorway-secondary-button = استفاده از تم قبلی
upgrade-dialog-colorway-theme-tooltip =
    .title = بررسی تم‌های پیش‌فرض
# $colorwayName (String) - Name of colorway, e.g., Abstract, Cheers
upgrade-dialog-colorway-colorway-tooltip =
    .title = بررسی رنگ‌بندی‌های { $colorwayName }
upgrade-dialog-colorway-default-theme = پیش‌فرض
# "Auto" is short for "Automatic"
upgrade-dialog-colorway-theme-auto = خودکار
    .title = دنبال کردن از تنظیمات سیستم برای دکمه‌ها، منوها و پنجره‌ها
upgrade-dialog-theme-light = روشن
    .title = از یک تم روشن برای دکمه‌ها، منوها و پنجره‌ها استفاده کنید
upgrade-dialog-theme-dark = تاریک
    .title = از یک تم تاریک برای دکمه‌ها، منوها و پنجره‌ها استفاده کنید.
upgrade-dialog-colorway-variation-soft = ملایم
    .title = استفاده از این رنگ‌بندی
upgrade-dialog-colorway-variation-balanced = متعادل
    .title = استفاده از این رنگ‌بندی
# "Bold" is used in the sense of bravery or courage, not in the sense of
# emphasized text.
upgrade-dialog-colorway-variation-bold = پُررنگ
    .title = استفاده از این رنگ‌بندی

## Thank you screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-thankyou-title = از اینکه ما را انتخاب کردید، متشکریم
upgrade-dialog-thankyou-subtitle = { -brand-short-name } یک مرورگر مستقل است که توسط یک سازمان غیرانتفاعی پشتیبانی می‌شود. با هم، ما وب را ایمن‌تر، سالم‌تر و خصوصی‌تر می‌کنیم.
upgrade-dialog-thankyou-primary-button = شروعِ وب‌گردی
