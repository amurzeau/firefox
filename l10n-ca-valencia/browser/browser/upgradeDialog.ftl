# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### Strings for the upgrade dialog that can be displayed on major version change.


## New changes screen

## Pin Firefox screen
##
## These title, subtitle and button strings differ between platforms as they
## match the OS' application context menu item action where Windows uses "pin"
## and "taskbar" while macOS "keep" and "Dock" (proper noun).

## Default browser screen

## Theme selection screen

## Start screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-start-title = La vida en colors
upgrade-dialog-start-subtitle = Combinacions de colors vibrants. Disponibles durant un temps limitat.
upgrade-dialog-start-primary-button = Exploreu les combinacions de colors
upgrade-dialog-start-secondary-button = Ara no

## Colorway screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-colorway-title = Trieu una paleta de colors
# This is shown to users with a custom home page, so they can switch to default.
upgrade-dialog-colorway-home-checkbox = Canvieu a l'Inici del Firefox amb un fons temàtic
upgrade-dialog-colorway-primary-button = Guarda la combinació de colors
upgrade-dialog-colorway-secondary-button = Conserva el tema anterior
upgrade-dialog-colorway-theme-tooltip =
    .title = Descobriu els temes per defecte.
# $colorwayName (String) - Name of colorway, e.g., Abstract, Cheers
upgrade-dialog-colorway-colorway-tooltip =
    .title = Descobriu les combinacions de colors de { $colorwayName }
upgrade-dialog-colorway-default-theme = Per defecte
# "Auto" is short for "Automatic"
upgrade-dialog-colorway-theme-auto = Automàtic
    .title = Utilitza el tema del sistema operatiu per als botons, menús i finestres
upgrade-dialog-theme-light = Clar
    .title = Utilitza un tema clar per als botons, menús i finestres
upgrade-dialog-theme-dark = Fosc
    .title = Utilitza un tema fosc per als botons, menús i finestres
upgrade-dialog-colorway-variation-soft = Suau
    .title = Utilitza esta combinació de colors
upgrade-dialog-colorway-variation-balanced = Equilibrat
    .title = Utilitza esta combinació de colors
# "Bold" is used in the sense of bravery or courage, not in the sense of
# emphasized text.
upgrade-dialog-colorway-variation-bold = Intrèpid
    .title = Utiliitza esta combinació de colors

## Thank you screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-thankyou-title = Gràcies per triar-nos
upgrade-dialog-thankyou-subtitle = El { -brand-short-name } és un navegador independent que té el suport d'una organització sense ànim de lucre. Tots junts, fem que el web siga més segur, més saludable i més privat.
upgrade-dialog-thankyou-primary-button = Comença a navegar
