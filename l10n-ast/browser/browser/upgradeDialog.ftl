# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### Strings for the upgrade dialog that can be displayed on major version change.


## New changes screen

## Pin Firefox screen
##
## These title, subtitle and button strings differ between platforms as they
## match the OS' application context menu item action where Windows uses "pin"
## and "taskbar" while macOS "keep" and "Dock" (proper noun).

## Default browser screen

## Theme selection screen

## Start screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-start-title = La vida a color
upgrade-dialog-start-secondary-button = Agora non

## Colorway screen

upgrade-dialog-colorway-secondary-button = Caltener l'estilu anterior
upgrade-dialog-theme-light = Claridá
    .title = Usa un estilu claru pa los botones, les ventanes y los menús
upgrade-dialog-theme-dark = Escuridá
    .title = Usa un estilu escuru pa los botones, les ventanes y los menús

## Thank you screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-thankyou-title = Gracies por escoyenos
upgrade-dialog-thankyou-subtitle = { -brand-short-name } ye un restolador independiente fechu por una organización ensin ánimu de llucru. Xuntos facemos una web más segura, sana y más privada.
upgrade-dialog-thankyou-primary-button = Comenzar a restolar
