# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### Strings for the upgrade dialog that can be displayed on major version change.


## New changes screen

## Pin Firefox screen
##
## These title, subtitle and button strings differ between platforms as they
## match the OS' application context menu item action where Windows uses "pin"
## and "taskbar" while macOS "keep" and "Dock" (proper noun).

## Default browser screen

## Theme selection screen

## Start screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-start-title = A vida en cor
upgrade-dialog-start-subtitle = Novas cores vibrantes. Dispoñible por tempo limitado.
upgrade-dialog-start-primary-button = Explore combinacións de cores
upgrade-dialog-start-secondary-button = Agora non

## Colorway screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-colorway-title = Escolla a súa paleta
# This is shown to users with a custom home page, so they can switch to default.
upgrade-dialog-colorway-home-checkbox = Pase ao Firefox Home con fondo temático
upgrade-dialog-colorway-primary-button = Gardar combinación de cores
upgrade-dialog-colorway-secondary-button = Manter tema anterior
upgrade-dialog-colorway-theme-tooltip =
    .title = Explorar temas predefinidos
# $colorwayName (String) - Name of colorway, e.g., Abstract, Cheers
upgrade-dialog-colorway-colorway-tooltip =
    .title = Explore combinacións de cores { $colorwayName }
upgrade-dialog-colorway-default-theme = Predefinido
# "Auto" is short for "Automatic"
upgrade-dialog-colorway-theme-auto = Automático
    .title = Seguir o tema do sistema operativo para os botóns, menús e xanelas
upgrade-dialog-theme-light = Claro
    .title = Empregar un tema claro para botóns, menús e xanelas
upgrade-dialog-theme-dark = Escuro
    .title = Empregar un tema escuro para botóns, menús e xanelas
upgrade-dialog-colorway-variation-soft = Suave
    .title = Empregar esta combinación de cores
upgrade-dialog-colorway-variation-balanced = Equilibrada
    .title = Empregar esta combinación de cores
# "Bold" is used in the sense of bravery or courage, not in the sense of
# emphasized text.
upgrade-dialog-colorway-variation-bold = Atrevida
    .title = Empregar esta combinación de cores

## Thank you screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-thankyou-title = Grazas por nos escoller
upgrade-dialog-thankyou-subtitle = O { -brand-short-name } é un navegador independente apoiado por unha organización sen ánimo de lucro. Xuntos, estamos a facer a web máis segura, sá e máis privada.
upgrade-dialog-thankyou-primary-button = Iniciar a navegación
