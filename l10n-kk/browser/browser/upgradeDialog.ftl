# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### Strings for the upgrade dialog that can be displayed on major version change.


## New changes screen

## Pin Firefox screen
##
## These title, subtitle and button strings differ between platforms as they
## match the OS' application context menu item action where Windows uses "pin"
## and "taskbar" while macOS "keep" and "Dock" (proper noun).

## Default browser screen

## Theme selection screen

## Start screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-start-title = Түрлі-түсті өмір
upgrade-dialog-start-subtitle = Жаңа, жарық түстер схемалары. Шектеулі уақыт ішінде ғана қолжетімді.
upgrade-dialog-start-primary-button = Түстер схемаларын шолу
upgrade-dialog-start-secondary-button = Қазір емес

## Colorway screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-colorway-title = Палитраңызды таңдаңыз
# This is shown to users with a custom home page, so they can switch to default.
upgrade-dialog-colorway-home-checkbox = Firefox үй парағы фонының түстерін таңдаңыз
upgrade-dialog-colorway-primary-button = Түстер схемасын сақтау
upgrade-dialog-colorway-secondary-button = Алдыңғы теманы қолдану
upgrade-dialog-colorway-theme-tooltip =
    .title = Бастапқы темаларды шолу.
# $colorwayName (String) - Name of colorway, e.g., Abstract, Cheers
upgrade-dialog-colorway-colorway-tooltip =
    .title = { $colorwayName } түстер схемаларын шолу
upgrade-dialog-colorway-default-theme = Бастапқы
# "Auto" is short for "Automatic"
upgrade-dialog-colorway-theme-auto = Автоматты түрде
    .title = Батырмалар, мәзірлер және терезелер үшін операциялық жүйе темасына сүйену
upgrade-dialog-theme-light = Ашық түсті
    .title = Батырмалар, мәзірлер және терезелер үшін ашық теманы қолдану
upgrade-dialog-theme-dark = Күңгірт
    .title = Батырмалар, мәзірлер және терезелер үшін күңгірт теманы қолдану
upgrade-dialog-colorway-variation-soft = Жұмсақ
    .title = Бұл түстер схемасын қолдану
upgrade-dialog-colorway-variation-balanced = Теңгерілген
    .title = Бұл түстер схемасын қолдану
# "Bold" is used in the sense of bravery or courage, not in the sense of
# emphasized text.
upgrade-dialog-colorway-variation-bold = Жуан
    .title = Бұл түстер схемасын қолдану

## Thank you screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-thankyou-title = Бізді таңдағаныңызға рахмет
upgrade-dialog-thankyou-subtitle = { -brand-short-name } — коммерциялық емес ұйым қолдайтын тәуелсіз браузер. Біз интернетті бірге қауіпсіз, сау және жеке етіп жасаймыз.
upgrade-dialog-thankyou-primary-button = Шолуды бастау
