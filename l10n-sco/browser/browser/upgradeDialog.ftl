# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### Strings for the upgrade dialog that can be displayed on major version change.


## New changes screen

## Pin Firefox screen
##
## These title, subtitle and button strings differ between platforms as they
## match the OS' application context menu item action where Windows uses "pin"
## and "taskbar" while macOS "keep" and "Dock" (proper noun).

## Default browser screen

## Theme selection screen

## Start screen

upgrade-dialog-start-secondary-button = No jist noo

## Colorway screen

upgrade-dialog-colorway-default-theme = Staunart
upgrade-dialog-theme-light = Licht
    .title = Yaise a licht theme fur buttons, menus, and windaes
upgrade-dialog-theme-dark = Daurk
    .title = Yaise a daurk theme fur buttons, menus, and windaes

## Thank you screen

upgrade-dialog-thankyou-primary-button = Stert stravaigin
