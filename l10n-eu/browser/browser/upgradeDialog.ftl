# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### Strings for the upgrade dialog that can be displayed on major version change.


## New changes screen

## Pin Firefox screen
##
## These title, subtitle and button strings differ between platforms as they
## match the OS' application context menu item action where Windows uses "pin"
## and "taskbar" while macOS "keep" and "Dock" (proper noun).

## Default browser screen

## Theme selection screen

## Start screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-start-title = Bizitza koloretan
upgrade-dialog-start-subtitle = Kolore-konbinazio bizi berriak. Denbora mugatuz erabilgarri.
upgrade-dialog-start-primary-button = Arakatu kolore-konbinazioak
upgrade-dialog-start-secondary-button = Orain ez

## Colorway screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-colorway-title = Aukeratu zure paleta
# This is shown to users with a custom home page, so they can switch to default.
upgrade-dialog-colorway-home-checkbox = Aldatu Firefoxen hasiera-orrira, itxuraren araberako atzeko planoarekin
upgrade-dialog-colorway-primary-button = Gorde kolore-konbinazioa
upgrade-dialog-colorway-secondary-button = Mantendu aurreko itxura
upgrade-dialog-colorway-theme-tooltip =
    .title = Arakatu itxura lehenetsiak
# $colorwayName (String) - Name of colorway, e.g., Abstract, Cheers
upgrade-dialog-colorway-colorway-tooltip =
    .title = Arakatu { $colorwayName } kolore-konbinazioak
upgrade-dialog-colorway-default-theme = Lehenetsia
# "Auto" is short for "Automatic"
upgrade-dialog-colorway-theme-auto = Automatikoa
    .title = Errespetatu sistema eragilearen itxura botoi, menu eta leihoetarako
upgrade-dialog-theme-light = Argia
    .title = Erabili itxura argia botoi, menu eta leihoetarako
upgrade-dialog-theme-dark = Iluna
    .title = Erabili itxura iluna botoi, menu eta leihoetarako
upgrade-dialog-colorway-variation-soft = Leuna
    .title = Erabili kolore-konbinazio hau
upgrade-dialog-colorway-variation-balanced = Orekatua
    .title = Erabili kolore-konbinazio hau
# "Bold" is used in the sense of bravery or courage, not in the sense of
# emphasized text.
upgrade-dialog-colorway-variation-bold = Bizia
    .title = Erabili kolore-konbinazio hau

## Thank you screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-thankyou-title = Eskerrik asko gu aukeratzeagatik
upgrade-dialog-thankyou-subtitle = Irabazi-asmorik gabeko erakunde batek babestutako nabigatzaile independentea da { -brand-short-name }. Elkarrekin weba seguruagoa, osasuntsuagoa eta pribatuagoa ari gara egiten.
upgrade-dialog-thankyou-primary-button = Hasi nabigatzen
