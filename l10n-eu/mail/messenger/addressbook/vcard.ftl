# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, you can obtain one at http://mozilla.org/MPL/2.0/.


# Display Name

vcard-displayname = Bistaratzeko izena
vcard-displayname-placeholder =
    .placeholder = { vcard-displayname }

# Type selection

vcard-entry-type-label = Mota
vcard-entry-type-home = Etxea
vcard-entry-type-work = Lana
vcard-entry-type-none = Bat ere ez
vcard-entry-type-custom = Pertsonalizatua

# N vCard field

vcard-name-header = Izena
vcard-n-prefix = Aurrizkia
vcard-n-add-prefix =
    .title = Gehitu aurrizkia
vcard-n-firstname = Izena
vcard-n-add-firstname =
    .title = Gehitu izena
vcard-n-middlename = Bigarren izena
vcard-n-add-middlename =
    .title = Gehitu bigarren izena
vcard-n-lastname = Abizenak
vcard-n-add-lastname =
    .title = Gehitu abizenak
vcard-n-suffix = Atzizkia
vcard-n-add-suffix =
    .title = Gehitu atzizkia

# Email vCard field

vcard-email-header = Helbide elektronikoak
vcard-email-add = Gehitu helbide elektronikoa
vcard-email-label = Helbide elektronikoa
vcard-primary-email-label = Lehenetsia

# URL vCard field

vcard-url-header = Webguneak
vcard-url-add = Gehitu webgunea
vcard-url-label = Webgunea

# Tel vCard field

vcard-tel-header = Telefono zenbakiak
vcard-tel-add = Gehitu telefono zenbakia
vcard-tel-label = Telefono zenbakia

# TZ vCard field

vcard-tz-header = Ordu-zona
vcard-tz-add = Gehitu ordu-zona

# IMPP vCard field

vcard-impp-header = Txat-kontuak
vcard-impp-add = Gehitu txat-kontua
vcard-impp-label = Txat-kontua

# BDAY and ANNIVERSARY vCard field

vcard-bday-anniversary-header = Data bereziak
vcard-bday-anniversary-add = Gehitu data berezia
vcard-bday-label = Urtebetetzea
vcard-anniversary-label = Urteurrena
vcard-date-day = Eguna
vcard-date-month = Hilabetea
vcard-date-year = Urtea

# ADR vCard field

vcard-adr-header = Helbideak
vcard-adr-add = Gehitu helbidea
vcard-adr-label = Helbidea
vcard-adr-delivery-label = Entrega etiketa
vcard-adr-pobox = Posta kutxa
vcard-adr-ext = Helbide guztia
vcard-adr-street = Helbideko kalea
# Or "Locality"
vcard-adr-locality = Hiria
# Or "Region"
vcard-adr-region = Estatua/probintzia
# The term "ZIP code" only applies in USA. Most locales should use "Postal code" only.
vcard-adr-code = ZIP/Posta Kodea:
vcard-adr-country = Herrialdea

# NOTE vCard field

vcard-note-header = Oharrak
vcard-note-add = Gehitu oharra

# TITLE, ROLE and ORGANIZATION vCard fields

vcard-org-header = Erakundearen propietateak
vcard-org-add = Gehitu erakundearen propietateak
vcard-org-title = Izenburua
vcard-org-role = Rola
vcard-org-org = Erakundea
