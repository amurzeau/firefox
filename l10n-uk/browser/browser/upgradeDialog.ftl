# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### Strings for the upgrade dialog that can be displayed on major version change.


## New changes screen

## Pin Firefox screen
##
## These title, subtitle and button strings differ between platforms as they
## match the OS' application context menu item action where Windows uses "pin"
## and "taskbar" while macOS "keep" and "Dock" (proper noun).

## Default browser screen

## Theme selection screen

## Start screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-start-title = Життя барвисте
upgrade-dialog-start-subtitle = Сповнені життя нові барви. Доступні впродовж обмеженого часу.
upgrade-dialog-start-primary-button = Переглянути забарвлення
upgrade-dialog-start-secondary-button = Не зараз

## Colorway screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-colorway-title = Оберіть свою палітру
upgrade-dialog-colorway-home-checkbox = Змінити тему тла домівки Firefox
upgrade-dialog-colorway-primary-button = Зберегти забарвлення
upgrade-dialog-colorway-secondary-button = Залишити попередню тему
upgrade-dialog-colorway-theme-tooltip =
    .title = Переглянути типові теми
# $colorwayName (String) - Name of colorway, e.g., Abstract, Cheers
upgrade-dialog-colorway-colorway-tooltip =
    .title = Переглянути забарвлення { $colorwayName }
upgrade-dialog-colorway-default-theme = Типові
# "Auto" is short for "Automatic"
upgrade-dialog-colorway-theme-auto = Авто
    .title = Дотримуватись теми операційної системи для кнопок, меню та вікон
upgrade-dialog-theme-light = Світла
    .title = Використовувати світлу тему для кнопок, меню та вікон
upgrade-dialog-theme-dark = Темна
    .title = Використовувати темну тему для кнопок, меню та вікон
upgrade-dialog-colorway-variation-soft = М'яке
    .title = Застосувати це забарвлення
upgrade-dialog-colorway-variation-balanced = Збалансоване
    .title = Застосувати це забарвлення
# "Bold" is used in the sense of bravery or courage, not in the sense of
# emphasized text.
upgrade-dialog-colorway-variation-bold = Насичене
    .title = Застосувати це забарвлення

## Thank you screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-thankyou-title = Дякуємо, що обрали нас
upgrade-dialog-thankyou-subtitle = { -brand-short-name } — незалежний браузер, підтримуваний некомерційною організацією. Разом ми робимо інтернет безпечнішим, здоровішим та приватнішим.
upgrade-dialog-thankyou-primary-button = Почати перегляд
