# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### Strings for the upgrade dialog that can be displayed on major version change.


## New changes screen

## Pin Firefox screen
##
## These title, subtitle and button strings differ between platforms as they
## match the OS' application context menu item action where Windows uses "pin"
## and "taskbar" while macOS "keep" and "Dock" (proper noun).

## Default browser screen

## Theme selection screen

## Start screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-start-title = La vida en color
upgrade-dialog-start-subtitle = Combinasons de colors vibrantas. Disponiblas durant un temps limitat.
upgrade-dialog-start-primary-button = Explorar los colorits
upgrade-dialog-start-secondary-button = Pas ara

## Colorway screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-colorway-title = Trapatz vòstra paleta
upgrade-dialog-colorway-home-checkbox = Passar a l’acuèlh de Firefox amb las colors de vòstre tèma
upgrade-dialog-colorway-primary-button = Salvar lo colorit
upgrade-dialog-colorway-secondary-button = Servar lo tèma precedent
upgrade-dialog-colorway-theme-tooltip =
    .title = Explorar los tèmas per defaut
# $colorwayName (String) - Name of colorway, e.g., Abstract, Cheers
upgrade-dialog-colorway-colorway-tooltip =
    .title = Veire lo colorit { $colorwayName }
upgrade-dialog-colorway-default-theme = Per defaut
# "Auto" is short for "Automatic"
upgrade-dialog-colorway-theme-auto = Auto
    .title = Seguir lo tèma del sistèma operatiu pels botons, menús e fenèstras
upgrade-dialog-theme-light = Clar
    .title = Utilizar un tèma clar pels botons, menús e las fenèstras
upgrade-dialog-theme-dark = Fosc
    .title = Utilizar un tèma fosc pels botons, menús e las fenèstras
upgrade-dialog-colorway-variation-soft = Leugièr
    .title = Utilizar aqueste colorit
upgrade-dialog-colorway-variation-balanced = Equilibrat
    .title = Utilizar aqueste colorit
# "Bold" is used in the sense of bravery or courage, not in the sense of
# emphasized text.
upgrade-dialog-colorway-variation-bold = Afortit
    .title = Utilizar aqueste colorit

## Thank you screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-thankyou-title = Mercés de nos aver causits
upgrade-dialog-thankyou-subtitle = { -brand-short-name } es un navegador independent sostengut per una organizacion sens tòca lucrativa. Amassa, fasèm que lo Web siá mai segur, sanitós e privat.
upgrade-dialog-thankyou-primary-button = Començar de navegar
