# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### Strings for the upgrade dialog that can be displayed on major version change.


## New changes screen

## Pin Firefox screen
##
## These title, subtitle and button strings differ between platforms as they
## match the OS' application context menu item action where Windows uses "pin"
## and "taskbar" while macOS "keep" and "Dock" (proper noun).

## Default browser screen

## Theme selection screen

## Start screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-start-title = Lífið í lit
upgrade-dialog-start-subtitle = Lífleg ný litasett. Í boði í takmarkaðan tíma.
upgrade-dialog-start-primary-button = Kannaðu litasett (colorways)
upgrade-dialog-start-secondary-button = Ekki núna

## Colorway screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-colorway-title = Veldu litaspjaldið þitt
# This is shown to users with a custom home page, so they can switch to default.
upgrade-dialog-colorway-home-checkbox = Skiptu yfir í Firefox-upphafssíðu með þemabakgrunni
upgrade-dialog-colorway-primary-button = Vista litasett
upgrade-dialog-colorway-secondary-button = Halda fyrra þema
upgrade-dialog-colorway-theme-tooltip =
    .title = Skoða sjálfgefin þemu
# $colorwayName (String) - Name of colorway, e.g., Abstract, Cheers
upgrade-dialog-colorway-colorway-tooltip =
    .title = Skoðaðu { $colorwayName } litasett
upgrade-dialog-colorway-default-theme = Sjálfgefið
# "Auto" is short for "Automatic"
upgrade-dialog-colorway-theme-auto = Sjálfvirkt
    .title = Fylgja þema kerfisins fyrir hnappa, valmyndir og glugga
upgrade-dialog-theme-light = Ljóst
    .title = Nota ljóst þema fyrir hnappa, valmyndir og glugga
upgrade-dialog-theme-dark = Dökkt
    .title = Nota dökkt þema fyrir hnappa, valmyndir og glugga
upgrade-dialog-colorway-variation-soft = Mjúkt
    .title = Nota þetta litasett
upgrade-dialog-colorway-variation-balanced = Jafnvægi
    .title = Nota þetta litasett
# "Bold" is used in the sense of bravery or courage, not in the sense of
# emphasized text.
upgrade-dialog-colorway-variation-bold = Djarft
    .title = Nota þetta litasett

## Thank you screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-thankyou-title = Þakka þér fyrir að velja okkur
upgrade-dialog-thankyou-subtitle = { -brand-short-name } er óháður vafri sem studdur er af sjálfseignarstofnun. Saman gerum við vefinn öruggari, heilbrigðari og persónulegri.
upgrade-dialog-thankyou-primary-button = Byrjaðu að vafra
