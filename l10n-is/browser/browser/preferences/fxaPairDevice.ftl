# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

fxa-pair-device-dialog =
    .title = Tengja annað tæki
    .style = width: 26em; min-height: 35em;

fxa-qrcode-heading-step1 = 1. Ef þú hefur ekki gert það nú þegar, skaltu setja upp <a data-l10n-name="connect-another-device">Firefox á farsímanum þínum</a>.

fxa-qrcode-heading-step2 = 2. Opnaðu Firefox á farsímanum þínum.

fxa-qrcode-heading-step3 = 3. Opnaðu <b> valmyndina </b> (<img data-l10n-name="ios-menu-icon" /> eða <img data-l10n-name="android-menu-icon"/>), ýttu á <img data-l10n-name="settings-icon"/> <b> Stillingar </b> og veldu <b>Kveikja á samstillingu</b>

fxa-qrcode-heading-step4 = 4. Skannaðu þennan kóða:

fxa-qrcode-error-title = Pörun tókst ekki.

fxa-qrcode-error-body = Reyndu aftur.
