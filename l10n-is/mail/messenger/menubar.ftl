# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

toolbar-context-menu-menu-bar =
    .toolbarname = Valmyndaslá
    .accesskey = m

## Tools Menu

menu-tools-settings =
    .label = Stillingar
    .accesskey = i
menu-addons-and-themes =
    .label = Viðbætur og þemu
    .accesskey = V

## Help Menu

menu-help-enter-troubleshoot-mode =
    .label = Úrræðaleitarhamur
    .accesskey = t
menu-help-exit-troubleshoot-mode =
    .label = Slökkva á úrræðaleitarham
    .accesskey = k
menu-help-more-troubleshooting-info =
    .label = Frekari upplýsingar um bilanaleit
    .accesskey = b

## Mail Toolbar

toolbar-junk-button =
    .label = Ruslpóstur
    .tooltiptext = Merkja valin skilaboð sem ruslpóst
toolbar-not-junk-button =
    .label = Ekki ruslpóstur
    .tooltiptext = Merkja valin skilaboð sem ekki-ruslpóst
toolbar-delete-button =
    .label = Eyða
    .tooltiptext = Eyða völdum skilaboðum eða möppum
toolbar-undelete-button =
    .label = Afturkalla eyðingu
    .tooltiptext = Afturkalla eyðingu á völdum skilaboðum

## View

menu-view-repair-text-encoding =
    .label = Gera við textakóðun
    .accesskey = x

## View / Layout

menu-font-size-label =
    .label = Leturstærð
    .accesskey = u
menuitem-font-size-enlarge =
    .label = Auka leturstærð
    .accesskey = A
menuitem-font-size-reduce =
    .label = Minnka leturstærð
    .accesskey = M
menuitem-font-size-reset =
    .label = Endurstilla leturstærð
    .accesskey = r
mail-uidensity-label =
    .label = Þéttleiki
    .accesskey = k
mail-uidensity-compact =
    .label = Þjappað
    .accesskey = p
mail-uidensity-normal =
    .label = Venjulegt
    .accesskey = n
mail-uidensity-touch =
    .label = Snertiskjár
    .accesskey = S
menu-spaces-toolbar-button =
    .label = Svæða-verkfæraslá
    .accesskey = S

## File

file-new-newsgroup-account =
    .label = Fréttareikningur…
    .accesskey = n
