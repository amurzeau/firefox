# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### Strings for the upgrade dialog that can be displayed on major version change.


## New changes screen

## Pin Firefox screen
##
## These title, subtitle and button strings differ between platforms as they
## match the OS' application context menu item action where Windows uses "pin"
## and "taskbar" while macOS "keep" and "Dock" (proper noun).

## Default browser screen

## Theme selection screen

## Start screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-start-title = Multkolora vivo
upgrade-dialog-start-subtitle = Novaj, vibraj koloraroj. Disponeblaj dum limigita tempo.
upgrade-dialog-start-primary-button = Esplori kolorarojn
upgrade-dialog-start-secondary-button = Ne nun

## Colorway screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-colorway-title = Elektu vian koloraron
# This is shown to users with a custom home page, so they can switch to default.
upgrade-dialog-colorway-home-checkbox = Ŝanĝi al la eka paĝo de Firefox kun la fono de via etoso
upgrade-dialog-colorway-primary-button = Konservi koloraron
upgrade-dialog-colorway-secondary-button = Gardi antaŭan etoson
upgrade-dialog-colorway-theme-tooltip =
    .title = Esplori normajn etosojn
# $colorwayName (String) - Name of colorway, e.g., Abstract, Cheers
upgrade-dialog-colorway-colorway-tooltip =
    .title = Esplori kolorarojn { $colorwayName }
upgrade-dialog-colorway-default-theme = Norma
# "Auto" is short for "Automatic"
upgrade-dialog-colorway-theme-auto = Aŭtomata
    .title = Sekvi la etoson de la mastruma sistemo por butonoj, menuoj kaj fenestroj
upgrade-dialog-theme-light = Hela
    .title = Uzi helan aspekton por butonoj, menuoj kaj fenestroj
upgrade-dialog-theme-dark = Malhela
    .title = Uzi malhelan aspekton por butonoj, menuoj kaj fenestroj
upgrade-dialog-colorway-variation-soft = Dolĉa
    .title = Uzi tiun ĉi koloraron
upgrade-dialog-colorway-variation-balanced = Ekvilibra
    .title = Uzi tiun ĉi koloraron
# "Bold" is used in the sense of bravery or courage, not in the sense of
# emphasized text.
upgrade-dialog-colorway-variation-bold = Kuraĝa
    .title = Uzi tiun ĉi koloraron

## Thank you screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-thankyou-title = Dankon pro tio ke vi nin elektis
upgrade-dialog-thankyou-subtitle = { -brand-short-name } estas sendependa retumilo subtenata de neprofitcela organizo. Kune, ni igas la reton pli sekura, pli sana kaj pli privata.
upgrade-dialog-thankyou-primary-button = Komenci retumi
