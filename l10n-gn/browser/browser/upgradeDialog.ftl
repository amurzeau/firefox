# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### Strings for the upgrade dialog that can be displayed on major version change.


## New changes screen

## Pin Firefox screen
##
## These title, subtitle and button strings differ between platforms as they
## match the OS' application context menu item action where Windows uses "pin"
## and "taskbar" while macOS "keep" and "Dock" (proper noun).

## Default browser screen

## Theme selection screen

## Start screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-start-title = Tekove sa’ýndi
upgrade-dialog-start-subtitle = Sa’y ojuehegua overáva pyahu. Eipurukuaáva sapy’ami.
upgrade-dialog-start-primary-button = Ehapereka sa’y ojueheguáva
upgrade-dialog-start-secondary-button = Ani ko’ág̃a

## Colorway screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-colorway-title = Eiporavo sa’yrenda
upgrade-dialog-colorway-home-checkbox = Emoambue Firefox kuatiarogue ñepyrũ ta’ãngarenda ndive
upgrade-dialog-colorway-primary-button = Eñongatu sa’y ojueheguáva
upgrade-dialog-colorway-secondary-button = Eguereko téma mboyvegua
upgrade-dialog-colorway-theme-tooltip =
    .title = Ehapereka téma ypyguáva
# $colorwayName (String) - Name of colorway, e.g., Abstract, Cheers
upgrade-dialog-colorway-colorway-tooltip =
    .title = Ehapereka sa’y ojueheguáva { $colorwayName }
upgrade-dialog-colorway-default-theme = Ijypykue
# "Auto" is short for "Automatic"
upgrade-dialog-colorway-theme-auto = Jehegui
    .title = Ehapykueho téma apopyvusu rehegua votõ, poravorã ha ovetã
upgrade-dialog-theme-light = Sakã
    .title = Eipuru téma sakã votõ, poravorã ha ovetãme g̃uarã
upgrade-dialog-theme-dark = Ypytũ
    .title = Eipuru téma ypytũ votõ, poravorã ha ovetãme g̃uarã
upgrade-dialog-colorway-variation-soft = Kangy
    .title = Eipuru ko ñembojopyru
upgrade-dialog-colorway-variation-balanced = Vavapyre
    .title = Eipuru ko ñembojopyru
# "Bold" is used in the sense of bravery or courage, not in the sense of
# emphasized text.
upgrade-dialog-colorway-variation-bold = Mohũ
    .title = Eipuru ko ñembojopyru

## Thank you screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-thankyou-title = Aguyje reimére orendive
upgrade-dialog-thankyou-subtitle = { -brand-short-name } ha’e kundahára hekosãsóva oykekóva chupe atyguasu viru’ỹgua. Oñondivepa jajapo ñanduti hekorosã, hesãi ha hekoñemíva.
upgrade-dialog-thankyou-primary-button = Eñepyrũ eikundaha
