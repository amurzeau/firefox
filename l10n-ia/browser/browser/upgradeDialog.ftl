# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### Strings for the upgrade dialog that can be displayed on major version change.


## New changes screen

## Pin Firefox screen
##
## These title, subtitle and button strings differ between platforms as they
## match the OS' application context menu item action where Windows uses "pin"
## and "taskbar" while macOS "keep" and "Dock" (proper noun).

## Default browser screen

## Theme selection screen

## Start screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-start-title = Vita in colores
upgrade-dialog-start-subtitle = Nove tonalitates vibrante. Disponibile pro tempore limitate.
upgrade-dialog-start-primary-button = Discoperir combinationes de colores
upgrade-dialog-start-secondary-button = Non ora

## Colorway screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-colorway-title = Elige tu combination de colores
# This is shown to users with a custom home page, so they can switch to default.
upgrade-dialog-colorway-home-checkbox = Passar al pagina initial de Firefox con un fundo a thema
upgrade-dialog-colorway-primary-button = Salvar combination de colores
upgrade-dialog-colorway-secondary-button = Mantener le previe thema
upgrade-dialog-colorway-theme-tooltip =
    .title = Discoperi le themas predefinite
# $colorwayName (String) - Name of colorway, e.g., Abstract, Cheers
upgrade-dialog-colorway-colorway-tooltip =
    .title = Discoperi le combinationes de colores { $colorwayName }
upgrade-dialog-colorway-default-theme = Predefinite
# "Auto" is short for "Automatic"
upgrade-dialog-colorway-theme-auto = Auto
    .title = Usa le mesme thema del systema operative pro buttones, menu e fenestras
upgrade-dialog-theme-light = Clar
    .title = Usar un thema clar pro buttones, menus e fenestras
upgrade-dialog-theme-dark = Obscur
    .title = Usar un thema obscur pro buttones, menus e fenestras
upgrade-dialog-colorway-variation-soft = Legier
    .title = Usa iste combination de colores
upgrade-dialog-colorway-variation-balanced = Balanciate
    .title = Usa iste combination de colores
# "Bold" is used in the sense of bravery or courage, not in the sense of
# emphasized text.
upgrade-dialog-colorway-variation-bold = Forte
    .title = Usa iste combination de colores

## Thank you screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-thankyou-title = Gratias pro haber eligite nos
upgrade-dialog-thankyou-subtitle = { -brand-short-name } es un navigator independente supportate per un organisation non-lucrative. Insimul, nos rende le web plus secur, plus salubre e plus private.
upgrade-dialog-thankyou-primary-button = Comenciar a navigar
