# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## View / Layout

appmenu-font-size-value = Dimension del litteras
appmenuitem-font-size-enlarge =
    .tooltiptext = Augmentar le dimension del litteras
appmenuitem-font-size-reduce =
    .tooltiptext = Reducer le dimension del litteras
# Variables:
# $size (String) - The current font size.
appmenuitem-font-size-reset =
    .label = { $size }px
    .tooltiptext = Remontar le dimension del litteras
