# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### Strings for the upgrade dialog that can be displayed on major version change.


## New changes screen

## Pin Firefox screen
##
## These title, subtitle and button strings differ between platforms as they
## match the OS' application context menu item action where Windows uses "pin"
## and "taskbar" while macOS "keep" and "Dock" (proper noun).

## Default browser screen

## Theme selection screen

## Start screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-start-title = Жыццё ў колеры
upgrade-dialog-start-subtitle = Яркія новыя колеры. Даступныя абмежаваны час.
upgrade-dialog-start-primary-button = Паглядзець расфарбоўкі
upgrade-dialog-start-secondary-button = Не зараз

## Colorway screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-colorway-title = Выберыце сваю палітру
# This is shown to users with a custom home page, so they can switch to default.
upgrade-dialog-colorway-home-checkbox = Пераключыцеся на стартавую старонку Firefox з тэматычным фонам
upgrade-dialog-colorway-primary-button = Захаваць расфарбоўку
upgrade-dialog-colorway-secondary-button = Пакінуць папярэднюю тэму
upgrade-dialog-colorway-theme-tooltip =
    .title = Паглядзець прадвызначаныя тэмы
# $colorwayName (String) - Name of colorway, e.g., Abstract, Cheers
upgrade-dialog-colorway-colorway-tooltip =
    .title = Паглядзець расфарбоўкі { $colorwayName }
upgrade-dialog-colorway-default-theme = Прадвызначаная
# "Auto" is short for "Automatic"
upgrade-dialog-colorway-theme-auto = Аўтаматычная
    .title = Прытрымлівацца колераў аперацыйнай сістэмы для кнопак, меню і акон
upgrade-dialog-theme-light = Светлая
    .title = Выкарыстоўваць светлую тэму для кнопак, меню і акон
upgrade-dialog-theme-dark = Цёмная
    .title = Выкарыстоўваць цёмную тэму для кнопак, меню і акон
upgrade-dialog-colorway-variation-soft = Мяккая
    .title = Выкарыстаць гэту расфарбоўку
upgrade-dialog-colorway-variation-balanced = Збалансаваная
    .title = Выкарыстаць гэту расфарбоўку
# "Bold" is used in the sense of bravery or courage, not in the sense of
# emphasized text.
upgrade-dialog-colorway-variation-bold = Выразная
    .title = Выкарыстаць гэту расфарбоўку

## Thank you screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-thankyou-title = Дзякуй, што выбралі нас
upgrade-dialog-thankyou-subtitle = { -brand-short-name } 一 незалежны браўзер, які падтрымліваецца некамерцыйнай арганізацыяй. Разам мы робім інтэрнэт больш бяспечным, здаровым і прыватным.
upgrade-dialog-thankyou-primary-button = Пачаць агляданне
