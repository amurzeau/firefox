# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### Strings for the upgrade dialog that can be displayed on major version change.


## New changes screen

## Pin Firefox screen
##
## These title, subtitle and button strings differ between platforms as they
## match the OS' application context menu item action where Windows uses "pin"
## and "taskbar" while macOS "keep" and "Dock" (proper noun).

## Default browser screen

## Theme selection screen

## Start screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-start-title = Život u boji
upgrade-dialog-start-subtitle = Žive nove boje. Dostupno na ograničeno vrijeme.
upgrade-dialog-start-primary-button = Istraži boje
upgrade-dialog-start-secondary-button = Ne sada

## Colorway screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-colorway-title = Odaberi svoju paletu
# This is shown to users with a custom home page, so they can switch to default.
upgrade-dialog-colorway-home-checkbox = Prijeđi na Firefoxovu početnu stranicu s tematskom pozadinom
upgrade-dialog-colorway-primary-button = Spremi boju
upgrade-dialog-colorway-secondary-button = Zadrži prethodnu temu
upgrade-dialog-colorway-theme-tooltip =
    .title = Istraži zadane teme
upgrade-dialog-theme-light = Svijetla
    .title = Koristi svijetlu temu za tipke, izbornike i prozore
upgrade-dialog-theme-dark = Tamna
    .title = Koristi tamnu temu za tipke, izbornike i prozore

## Thank you screen

upgrade-dialog-thankyou-primary-button = Započnite surfati
