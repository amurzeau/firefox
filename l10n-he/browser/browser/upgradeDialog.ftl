# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### Strings for the upgrade dialog that can be displayed on major version change.


## New changes screen

## Pin Firefox screen
##
## These title, subtitle and button strings differ between platforms as they
## match the OS' application context menu item action where Windows uses "pin"
## and "taskbar" while macOS "keep" and "Dock" (proper noun).

## Default browser screen

## Theme selection screen

## Start screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-start-title = החיים בצבע
upgrade-dialog-start-subtitle = ערכות צבעים חדשים ותוססים. זמינים לזמן מוגבל.
upgrade-dialog-start-primary-button = עיון בערכות צבעים
upgrade-dialog-start-secondary-button = לא כעת

## Colorway screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-colorway-title = בחירת ערכת הצבעים שלך
# This is shown to users with a custom home page, so they can switch to default.
upgrade-dialog-colorway-home-checkbox = מעבר למסך הבית של Firefox עם רקע של ערכת הנושא
upgrade-dialog-colorway-primary-button = שמירת ערכת הצבעים
upgrade-dialog-colorway-secondary-button = שמירת ערכת הנושא הקודמת
upgrade-dialog-colorway-theme-tooltip =
    .title = עיון בערכות נושא ברירת מחדל
# $colorwayName (String) - Name of colorway, e.g., Abstract, Cheers
upgrade-dialog-colorway-colorway-tooltip =
    .title = עיון בערכות צבעים מסוג { $colorwayName }
upgrade-dialog-colorway-default-theme = ברירת מחדל
# "Auto" is short for "Automatic"
upgrade-dialog-colorway-theme-auto = אוטומטי
    .title = עוקב אחר הגדרת מערכת ההפעלה עבור כפתורים, תפריטים וחלונות.
upgrade-dialog-theme-light = בהירה
    .title = שימוש בערכת נושא בהירה עבור כפתורים, תפריטים וחלונות
upgrade-dialog-theme-dark = כהה
    .title = שימוש בערכת נושא כהה עבור כפתורים, תפריטים וחלונות
upgrade-dialog-colorway-variation-soft = רך
    .title = שימוש בערכת צבעים זו
upgrade-dialog-colorway-variation-balanced = מאוזן
    .title = שימוש בערכת צבעים זו
# "Bold" is used in the sense of bravery or courage, not in the sense of
# emphasized text.
upgrade-dialog-colorway-variation-bold = נועז
    .title = שימוש בערכת צבעים זו

## Thank you screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-thankyou-title = תודה רבה שבחרת בנו
upgrade-dialog-thankyou-subtitle = ‏{ -brand-short-name } הוא דפדפן עצמאי המגובה על־ידי עמותה שאינה למטרות רווח. יחד, אנו הופכים את האינטרנט לבטוח, בריא ופרטי יותר. 
upgrade-dialog-thankyou-primary-button = התחלת גלישה
