# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, you can obtain one at http://mozilla.org/MPL/2.0/.


# Type selection


# N vCard field

vcard-n-prefix = Prefix
vcard-n-add-prefix =
    .title = Add prefix
vcard-n-firstname = First name
vcard-n-add-firstname =
    .title = Add first name
vcard-n-middlename = Middle name
vcard-n-add-middlename =
    .title = Add middle name
vcard-n-lastname = Last name
vcard-n-add-lastname =
    .title = Add last name
vcard-n-suffix = Suffix
vcard-n-add-suffix =
    .title = Add suffix

# Email vCard field


# URL vCard field


# Tel vCard field


# TZ vCard field

