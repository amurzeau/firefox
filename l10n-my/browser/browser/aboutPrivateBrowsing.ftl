# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

privatebrowsingpage-open-private-window-label = သီးသန့်ဝင်းဒိုးကို ဖွင့်ပါ
    .accesskey = P
about-private-browsing-search-placeholder = ဝဘ်တွင် ရှာဖွေပါ
about-private-browsing-info-title = သင်ဟာ ကိုယ်ပိုင်ဝင်းဒိုးရှာဖွေမှုထဲတွင်ရှိပါသည်။
about-private-browsing-not-private = သင်သည် လက်ရှိတွင် သီးသန့်ဝင်းဒိုးတွင် ရှိမနေပါ။

## The following strings will be used for experiments in Fx99 and Fx100

# This string is the title for the banner for search engine selection
# in a private window.
# Variables:
#   $engineName (String) - The engine name that will currently be used for the private window.
about-private-browsing-search-banner-title = { $engineName } သည် သီးသန့် ဝင်းဒိုးများတွင် သင်၏ ပုံမှန် ရှာဖွေရေးအင်ဂျင်ဖြစ်သည်။
about-private-browsing-search-banner-close-button =
    .aria-label = ပိတ်ရန်

