# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### Strings for the upgrade dialog that can be displayed on major version change.


## New changes screen

## Pin Firefox screen
##
## These title, subtitle and button strings differ between platforms as they
## match the OS' application context menu item action where Windows uses "pin"
## and "taskbar" while macOS "keep" and "Dock" (proper noun).

## Default browser screen

## Theme selection screen

## Start screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-start-title = Живот в цвят
upgrade-dialog-start-subtitle = Изразителни цветни комбинации. Достъпни за ограничен период.
upgrade-dialog-start-primary-button = Разглеждане на цветови комбинации
upgrade-dialog-start-secondary-button = Не сега

## Colorway screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-colorway-title = Изберете вашата палитра
# This is shown to users with a custom home page, so they can switch to default.
upgrade-dialog-colorway-home-checkbox = Превключете към Firefox Home с тематичен фон
upgrade-dialog-colorway-primary-button = Запазване на палитра
upgrade-dialog-colorway-secondary-button = Запазване предишната тема
upgrade-dialog-colorway-theme-tooltip =
    .title = Разгледайте темите по подразбиране
# $colorwayName (String) - Name of colorway, e.g., Abstract, Cheers
upgrade-dialog-colorway-colorway-tooltip =
    .title = Разгледайте цветовите комбинации на { $colorwayName }
upgrade-dialog-colorway-default-theme = По подразбиране
# "Auto" is short for "Automatic"
upgrade-dialog-colorway-theme-auto = Автоматично
    .title = Следва темата на операционната система за бутони, менюта и прозорци
upgrade-dialog-theme-light = Светла
    .title = Светла тема за бутони, менюта и прозорци
upgrade-dialog-theme-dark = Тъмна
    .title = Тъмна тема за бутони, менюта и прозорци
upgrade-dialog-colorway-variation-soft = Мека
    .title = Използвайте тази цветна комбинация
upgrade-dialog-colorway-variation-balanced = Умерена
    .title = Използвайте тази цветна комбинация
# "Bold" is used in the sense of bravery or courage, not in the sense of
# emphasized text.
upgrade-dialog-colorway-variation-bold = Ярка
    .title = Използвайте тази цветна комбинация

## Thank you screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-thankyou-title = Благодарим ви, че избрахте нас
upgrade-dialog-thankyou-subtitle = { -brand-short-name } е независим четец, поддържан от организация с нестопанска цел. Заедно правим мрежата, по-безопасна, здрава и поверителна.
