# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### These strings are used inside the Storage Inspector.

# Key shortcut used to focus the filter box on top of the data view
storage-filter-key = CmdOrCtrl+F

# Placeholder for the searchbox that allows you to filter the table items
storage-search-box =
    .placeholder = අයිතම පෙරන්න

# Placeholder text in the sidebar search box
storage-variable-view-search-box =
    .placeholder = අගයන් පෙරන්න

# Context menu action to delete all storage items
storage-context-menu-delete-all =
    .label = සියල්ල මකන්න

## Header names of the columns in the Storage Table for each type of storage available
## through the Storage Tree to the side.

storage-table-headers-cookies-name = නම
storage-table-headers-cookies-value = අගය
storage-table-headers-cache-status = තත්ත්වය

## Labels for Storage type groups present in the Storage Tree, like cookies, local storage etc.

storage-tree-labels-cookies = කුකී
storage-tree-labels-local-storage = දේශීය ආචයන
storage-tree-labels-session-storage = වාර ආචයනය
storage-tree-labels-indexed-db = සූචිගත DB

##

# String displayed in the expires column when the cookie is a Session Cookie
storage-expires-session = සැසිය

# Heading displayed over the item value in the sidebar
storage-data = දත්ත

# Heading displayed over the item parsed value in the sidebar
storage-parsed-value = ඛණ්ඩනය කළ අගය

