# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, you can obtain one at http://mozilla.org/MPL/2.0/.

import-page-title = Importar

## Header

import-from-app = Importar dad ina applicaziun

import-from-app-desc = Tscherna dad importar contos, cudeschets d'adressas, chalenders ed autras datas da:

import-address-book = Importar ina datoteca cun in cudeschet d'adressas

import-calendar = Importar ina datoteca da chalender

export-profile = Exportar

## Buttons

button-cancel = Interrumper

button-back = Enavos

button-continue = Cuntinuar

button-export = Exportar

## Import from app steps

app-name-thunderbird = Thunderbird

app-name-seamonkey = SeaMonkey

app-name-outlook = Outlook

app-name-becky = Becky! Internet Mail

app-name-apple-mail = Apple Mail

## Import from file selections

# Variables:
#   $app (String) - The name of the app to import from
profiles-pane-title = Importar da { $app }

profiles-pane-desc = Tscherna la posiziun d'origin per l'import

profile-file-picker-dir = Tscherna in ordinatur da profil

profile-file-picker-zip = Tscherna ina datoteca zip (che na surpassa betg 2GB)

items-pane-title = Tscherna tge importar

items-pane-source = Lieu da funtauna:

items-pane-checkbox-accounts = Contos e parameters

items-pane-checkbox-address-books = Cudeschets d'adressas

items-pane-checkbox-calendars = Chalenders

items-pane-checkbox-mail-messages = Messadis dad e-mail

## Import from address book file steps

import-from-addr-book-file-desc = Tscherna il tip da datoteca che ti vulessas importar:

addr-book-csv-file = Datoteca separada cun commas u tabulaturs (.csv, .tsv)

addr-book-ldif-file = Datoteca LDIF (.ldif)

addr-book-vcard-file = Datoteca vCard (.vcf, .vcard)

addr-book-sqlite-file = Datoteca da banca da datas SQLite (.sqlite)

addr-book-mab-file = Datoteca da la banca da datas Mork (.mab)

addr-book-file-picker = Tscherna ina datoteca da cudeschet d'adressas

addr-book-csv-field-map-title = Attribuir ils nums dals champs

addr-book-csv-field-map-desc = Tscherna ils champs dal cudeschet d'adressas che correspundan als champs da la funtauna. Champs betg selecziunads na vegnan betg importads.

addr-book-directories-pane-title = Tscherna l'ordinatur en il qual ti vulessas importar:

addr-book-directories-pane-source = Datoteca da funtauna:

addr-book-import-into-new-directory = Crear in nov ordinatur

## Import from address book file steps

import-from-calendar-file-desc = Tscherna la datoteca iCalendar (.ics) che ti vuls importar.

calendar-items-loading = Chargiar ils elements…

calendar-items-filter-input =
    .placeholder = Filtrar ils elements…

calendar-select-all-items = Selecziunar tut

calendar-deselect-all-items = Deselecziunar tut

calendar-import-into-new-calendar = Crear in nov chalender

## Import dialog

progress-pane-importing = Importar

progress-pane-exporting = Exportar

progress-pane-finished-desc = Finì.

progress-pane-restart-desc = Reaviar per finir l'import.

error-pane-title = Errur

error-message-zip-file-too-big = La datoteca zip tschernida surpassa 2GB. L'extira l'emprim ed importescha lura ord l'ordinatur extratg.

error-message-extract-zip-file-failed = I n'è betg reussì dad extrair la datoteca zip. L'extira per plaschair manualmain ed importescha lura ord l'ordinatur extratg.

error-message-failed = L'import n'è betg reussì nunspetgadamain. Ulteriuras infurmaziuns stattan eventualmain a disposiziun en la consola d'errurs.

error-failed-to-parse-ics-file = Na chattà nagins elements en la datoteca che sa laschan importar.

error-export-failed = L'export n'è nunspetgadamain betg reussì. Ulteriuras infurmaziuns stattan eventualmain a disposiziun en la consola d'errurs.

## <csv-field-map> element

csv-first-row-contains-headers = L'emprima lingia cuntegna ils nums dals champs

csv-source-field = Champ da funtauna

csv-source-first-record = Emprima endataziun

csv-source-second-record = Segunda endataziun

csv-target-field = Champ dal cudeschet d'adressas

## Export tab

export-profile-desc = Exportar contos dad e-mail, messadis dad e-mail, cudeschets d'adressas e parameters en ina datoteca zip. En cas da necessitad pos ti importar la datoteca zip per restaurar tes profil.

export-profile-desc2 = Sche tes profil actual è pli grond che 2GB, ta recumandain nus da sez far la copia da segirezza.

export-open-profile-folder = Avrir l'ordinatur dal profil

export-file-picker = Exportar en ina datoteca zip

export-brand-name = { -brand-product-name }
