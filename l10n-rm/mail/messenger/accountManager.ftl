# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

open-preferences-sidebar-button2 = Parameters da { -brand-short-name }

open-addons-sidebar-button = Supplements e designs

account-action-add-newsgroup-account =
    .label = Agiuntar in conto da gruppas da discussiun…
    .accesskey = n

