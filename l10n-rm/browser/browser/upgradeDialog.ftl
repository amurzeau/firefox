# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### Strings for the upgrade dialog that can be displayed on major version change.


## New changes screen

## Pin Firefox screen
##
## These title, subtitle and button strings differ between platforms as they
## match the OS' application context menu item action where Windows uses "pin"
## and "taskbar" while macOS "keep" and "Dock" (proper noun).

## Default browser screen

## Theme selection screen

## Start screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-start-title = Ina vita en colurs
upgrade-dialog-start-subtitle = Ina nova cumbinaziun da colurs vivas. Disponibla durant in temp limità.
upgrade-dialog-start-primary-button = Scuvra las cumbinaziuns da colurs
upgrade-dialog-start-secondary-button = Betg ussa

## Colorway screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-colorway-title = Tscherna tia paletta
upgrade-dialog-colorway-home-checkbox = Mida a la pagina da partenza da Firefox cun in fund davos da design
upgrade-dialog-colorway-primary-button = Memorisar la cumbinaziun da colurs
upgrade-dialog-colorway-secondary-button = Mantegnair il design precedent
upgrade-dialog-colorway-theme-tooltip =
    .title = Scuvrir ils designs predefinids
# $colorwayName (String) - Name of colorway, e.g., Abstract, Cheers
upgrade-dialog-colorway-colorway-tooltip =
    .title = Scuvrir la cumbinaziun da colurs { $colorwayName }
upgrade-dialog-colorway-default-theme = Standard
# "Auto" is short for "Automatic"
upgrade-dialog-colorway-theme-auto = Automatic
    .title = S'adattar al sistem operativ areguard il design da buttuns, menus e fanestras
upgrade-dialog-theme-light = Cler
    .title = Utilisescha in design cler per buttuns, menus e fanestras
upgrade-dialog-theme-dark = Stgir
    .title = Utilisescha in design stgir per buttuns, menus e fanestras
upgrade-dialog-colorway-variation-soft = Bufatg
    .title = Utilisar questa cumbinaziun da colurs
upgrade-dialog-colorway-variation-balanced = Equilibrà
    .title = Utilisar questa cumbinaziun da colurs
# "Bold" is used in the sense of bravery or courage, not in the sense of
# emphasized text.
upgrade-dialog-colorway-variation-bold = Ferm
    .title = Utilisar questa cumbinaziun da colurs

## Thank you screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-thankyou-title = Grazia per avair tschernì nus
upgrade-dialog-thankyou-subtitle = { -brand-short-name } è in navigatur independent dad ina organisaziun senza finamira da profit. Communablamain rendain nus il web pli segir, pli saun e pli privat.
upgrade-dialog-thankyou-primary-button = Cumenzar a navigar
