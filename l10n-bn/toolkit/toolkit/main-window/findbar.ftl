# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### This file contains the entities needed to use the Find Bar.

findbar-next =
    .tooltiptext = বাক্যাংশের পরবর্তী উপস্থিতি খুঁজুন
findbar-previous =
    .tooltiptext = বাক্যাংশের পূর্ববর্তী উপস্থিতি খুঁজুন
findbar-find-button-close =
    .tooltiptext = খোঁজার দণ্ড বন্ধ করুন
findbar-highlight-all2 =
    .label = সব হাইলাইট করুন
    .accesskey =
        { PLATFORM() ->
            [macos] l
           *[other] a
        }
    .tooltiptext = এই বাক্যের সকল আবির্ভাব হাইলাইট করুন
findbar-case-sensitive =
    .label = অক্ষরের ছাঁদ মিলান
    .accesskey = C
    .tooltiptext = অক্ষরের ছাঁদসহ অনুসন্ধান করুন
findbar-match-diacritics =
    .label = বৈশিষ্ট্যসূচক চিহ্ন মিলান
    .accesskey = i
    .tooltiptext = উচ্চারিত অক্ষর ও তাদের মূল অক্ষরের মধ্যে পার্থক্য খুঁজে বের করুন (যেমন, যদি আপনি “resume” খুঁজেন, সেক্ষেত্রে “résumé” সেটির সাথে মিলবে না)
findbar-entire-word =
    .label = সম্পূর্ণ শব্দ
    .accesskey = W
    .tooltiptext = শুধুমাত্র সম্পূর্ণ শব্দ অনুসন্ধান করুন
