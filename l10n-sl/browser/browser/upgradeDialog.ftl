# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### Strings for the upgrade dialog that can be displayed on major version change.


## New changes screen

## Pin Firefox screen
##
## These title, subtitle and button strings differ between platforms as they
## match the OS' application context menu item action where Windows uses "pin"
## and "taskbar" while macOS "keep" and "Dock" (proper noun).

## Default browser screen

## Theme selection screen

## Start screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-start-title = Barvito življenje
upgrade-dialog-start-subtitle = Živahne nove barvne kombinacije. Le še kratek čas.
upgrade-dialog-start-primary-button = Raziščite barvne kombinacije
upgrade-dialog-start-secondary-button = Ne zdaj

## Colorway screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-colorway-title = Izberite si paleto
# This is shown to users with a custom home page, so they can switch to default.
upgrade-dialog-colorway-home-checkbox = Preklopi na domačo stran Firefoxa s temo v ozadju
upgrade-dialog-colorway-primary-button = Shrani barvno kombinacijo
upgrade-dialog-colorway-secondary-button = Obdrži dosedanjo temo
upgrade-dialog-colorway-theme-tooltip =
    .title = Raziščite privzete teme
# $colorwayName (String) - Name of colorway, e.g., Abstract, Cheers
upgrade-dialog-colorway-colorway-tooltip =
    .title = Raziščite barvne kombinacije za { $colorwayName }
upgrade-dialog-colorway-default-theme = Privzeta
# "Auto" is short for "Automatic"
upgrade-dialog-colorway-theme-auto = Samodejno
    .title = Sledi nastavitvam operacijskega sistema za gumbe, menije in okna
upgrade-dialog-theme-light = Svetla
    .title = Uporabi svetlo temo za gumbe, menije in okna
upgrade-dialog-theme-dark = Temna
    .title = Uporabi temno temo za gumbe, menije in okna
upgrade-dialog-colorway-variation-soft = Mehka
    .title = Uporabi to barvno kombinacijo
upgrade-dialog-colorway-variation-balanced = Uravnotežena
    .title = Uporabi to barvno kombinacijo
# "Bold" is used in the sense of bravery or courage, not in the sense of
# emphasized text.
upgrade-dialog-colorway-variation-bold = Drzna
    .title = Uporabi to barvno kombinacijo

## Thank you screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-thankyou-title = Hvala, ker ste nas izbrali
upgrade-dialog-thankyou-subtitle = { -brand-short-name } je neodvisen brskalnik, za katerim stoji neprofitna organizacija. Skupaj ustvarjamo splet varnejši, zasebnejši in bolj zdrav.
upgrade-dialog-thankyou-primary-button = Začnite z brskanjem
