# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

toolbar-context-menu-menu-bar =
    .toolbarname = Vrstica z menijem
    .accesskey = m

## Tools Menu

menu-tools-settings =
    .label = Nastavitve
    .accesskey = t
menu-addons-and-themes =
    .label = Dodatki in teme
    .accesskey = D

## Help Menu

menu-help-enter-troubleshoot-mode =
    .label = Način za odpravljanje težav …
    .accesskey = r
menu-help-exit-troubleshoot-mode =
    .label = Izključi način za odpravljanje težav
    .accesskey = I
menu-help-more-troubleshooting-info =
    .label = Več podatkov za odpravljanje težav
    .accesskey = V

## Mail Toolbar

toolbar-junk-button =
    .label = Neželeno
    .tooltiptext = Označi izbrana sporočila kot neželena
toolbar-not-junk-button =
    .label = Ni neželeno
    .tooltiptext = Označi izbrana sporočila kot želena
toolbar-delete-button =
    .label = Izbriši
    .tooltiptext = Izbriši izbrana sporočila ali mapo
toolbar-undelete-button =
    .label = Prekliči brisanje
    .tooltiptext = Razveljavi izbris izbranih sporočil

## View

menu-view-repair-text-encoding =
    .label = Popravi kodiranje besedila
    .accesskey = r

## View / Layout

menu-font-size-label =
    .label = Velikost pisave
    .accesskey = o
menuitem-font-size-enlarge =
    .label = Povečaj velikost pisave
    .accesskey = v
menuitem-font-size-reduce =
    .label = Zmanjšaj velikost pisave
    .accesskey = m
menuitem-font-size-reset =
    .label = Ponastavi velikost pisave
    .accesskey = n
mail-uidensity-label =
    .label = Gostota
    .accesskey = G
mail-uidensity-compact =
    .label = Strnjeno
    .accesskey = S
mail-uidensity-normal =
    .label = Običajno
    .accesskey = O
mail-uidensity-touch =
    .label = Dotik
    .accesskey = D
menu-spaces-toolbar-button =
    .label = Vrstica komponent
    .accesskey = k

## File

file-new-newsgroup-account =
    .label = Račun za novičarsko skupino …
    .accesskey = č
