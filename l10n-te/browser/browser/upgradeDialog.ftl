# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### Strings for the upgrade dialog that can be displayed on major version change.


## New changes screen

## Pin Firefox screen
##
## These title, subtitle and button strings differ between platforms as they
## match the OS' application context menu item action where Windows uses "pin"
## and "taskbar" while macOS "keep" and "Dock" (proper noun).

## Default browser screen

## Theme selection screen


## Start screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-start-title = జీవితం రంగులమయం
upgrade-dialog-start-secondary-button = ఇప్పుడు కాదు

## Colorway screen

upgrade-dialog-colorway-secondary-button = మునుపటి అలంకారాన్ని ఉండనివ్వు
upgrade-dialog-colorway-theme-tooltip =
    .title = అప్రమేయ అలంకారాలను చూడండి
upgrade-dialog-colorway-default-theme = అప్రమేయం

## Thank you screen

upgrade-dialog-thankyou-primary-button = విహరించడం మొదలుపెట్టండి
