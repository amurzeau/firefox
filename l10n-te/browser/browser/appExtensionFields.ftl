# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Theme names and descriptions used in the Themes panel in about:addons

# "Auto" is short for automatic. It can be localized without limitations.
extension-default-theme-name-auto = వ్యవస్థ అలంకారం — స్వీయం

extension-firefox-compact-light-name = లేత
extension-firefox-compact-light-description = లేత రంగులో అలంకారం.

extension-firefox-compact-dark-name = ముదురు
extension-firefox-compact-dark-description = ముదురు రంగు అలంకారం.

## Colorway Themes
## These themes are variants of a colorway. The colorway is specified in the
## $colorway-name variable.
## Variables
##   $colorway-name (String) The name of a colorway (e.g. Graffiti, Elemental).

extension-colorways-soft-name = { $colorway-name } — కోమలం
extension-colorways-balanced-name = { $colorway-name } — సమతుల్యం
# "Bold" is used in the sense of bravery or courage, not in the sense of
# emphasized text.
extension-colorways-bold-name = { $colorway-name } — గంభీరం
