# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

toolbar-context-menu-menu-bar =
    .toolbarname = Bar Dewislen
    .accesskey = D

## Tools Menu

menu-tools-settings =
    .label = Gosodiadau
    .accesskey = G
menu-addons-and-themes =
    .label = Ychwanegion a Themâu
    .accesskey = Y

## Help Menu

menu-help-enter-troubleshoot-mode =
    .label = Y Modd Datrys Problemau…
    .accesskey = M
menu-help-exit-troubleshoot-mode =
    .label = Diffodd y Modd Dartrys Problemau
    .accesskey = D
menu-help-more-troubleshooting-info =
    .label = Rhagor o Wybodaeth Datrys Problemau
    .accesskey = R

## Mail Toolbar

toolbar-junk-button =
    .label = Sbwriel
    .tooltiptext = Marcio'r negeseuon hyn fel sbwriel
toolbar-not-junk-button =
    .label = Nid Sbwriel
    .tooltiptext = Marcio'r negeseuon hyn fel nid sbwriel
toolbar-delete-button =
    .label = Dileu
    .tooltiptext = Dileu'r negeseuon neu ffolderi hyn
toolbar-undelete-button =
    .label = Dad-ddileu
    .tooltiptext = Dad-ddileu'r negeseuon hyn

## View

menu-view-repair-text-encoding =
    .label = Trwsio Amgodio Testun
    .accesskey = T

## View / Layout

menu-font-size-label =
    .label = Maint Ffont
    .accesskey = M
menuitem-font-size-enlarge =
    .label = Cynyddu Maint Ffont
    .accesskey = F
menuitem-font-size-reduce =
    .label = Lleihau Maint Ffont
    .accesskey = L
menuitem-font-size-reset =
    .label = Ailosod Maint Ffont
    .accesskey = A
mail-uidensity-label =
    .label = Dwysedd
    .accesskey = D
mail-uidensity-compact =
    .label = Cryno
    .accesskey = C
mail-uidensity-normal =
    .label = Arferol
    .accesskey = A
mail-uidensity-touch =
    .label = Cyffwrdd
    .accesskey = y
menu-spaces-toolbar-button =
    .label = Bar Offer Mannau
    .accesskey = B

## File

file-new-newsgroup-account =
    .label = Cyfrif Grŵp Newyddion…
    .accesskey = N
