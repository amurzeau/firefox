# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### Strings for the upgrade dialog that can be displayed on major version change.


## New changes screen

## Pin Firefox screen
##
## These title, subtitle and button strings differ between platforms as they
## match the OS' application context menu item action where Windows uses "pin"
## and "taskbar" while macOS "keep" and "Dock" (proper noun).

## Default browser screen

## Theme selection screen

## Start screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-start-title = Bywyd mewn lliw
upgrade-dialog-start-subtitle = Llwybrau lliw newydd bywiog. Ar gael am gyfnod cyfyngedig.
upgrade-dialog-start-primary-button = Archwilio'r llwybrau lliw
upgrade-dialog-start-secondary-button = Nid nawr

## Colorway screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-colorway-title = Dewis eich palet
upgrade-dialog-colorway-home-checkbox = Newid i Cartref Firefox gyda chefndir y thema
upgrade-dialog-colorway-primary-button = Cadw'r llwybr lliw
upgrade-dialog-colorway-secondary-button = Cadw'r thema flaenorol
upgrade-dialog-colorway-theme-tooltip =
    .title = Archwilio'r themâu rhagosodedig.
# $colorwayName (String) - Name of colorway, e.g., Abstract, Cheers
upgrade-dialog-colorway-colorway-tooltip =
    .title = Archwilio llwybrau lliw { $colorwayName }
upgrade-dialog-colorway-default-theme = Rhagosodedig
# "Auto" is short for "Automatic"
upgrade-dialog-colorway-theme-auto = Awto
    .title = Dilyn thema'r system weithredu ar fotymau, dewislenni a ffenestri
upgrade-dialog-theme-light = Golau
    .title = Defnyddio thema olau ar gyfer botymau, dewislenni a ffenestri
upgrade-dialog-theme-dark = Tywyll
    .title = Defnyddio thema dywyll ar gyfer botymau, dewislenni a ffenestri
upgrade-dialog-colorway-variation-soft = Meddal
    .title = Defnyddio'r llwybr lliw yma
upgrade-dialog-colorway-variation-balanced = Cytbwys
    .title = Defnyddio'r llwybr lliw yma
# "Bold" is used in the sense of bravery or courage, not in the sense of
# emphasized text.
upgrade-dialog-colorway-variation-bold = Beiddgar
    .title = Defnyddio'r llwybr lliw yma

## Thank you screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-thankyou-title = Diolch am ein dewis ni
upgrade-dialog-thankyou-subtitle = Mae { -brand-short-name } yn borwr annibynnol gyda chefnogaeth corff dim-er-elw. Gyda'n gilydd, rydyn ni'n gwneud y we yn ddiogelach, iachach a mwy preifat.
upgrade-dialog-thankyou-primary-button = Cychwyn pori
