# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### Strings for the upgrade dialog that can be displayed on major version change.


## New changes screen

## Pin Firefox screen
##
## These title, subtitle and button strings differ between platforms as they
## match the OS' application context menu item action where Windows uses "pin"
## and "taskbar" while macOS "keep" and "Dock" (proper noun).

## Default browser screen

## Theme selection screen

## Start screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-start-title = Tudert ifeǧǧeǧen
upgrade-dialog-start-subtitle = Tafrant n yiniten tamaynut i d-yettakken rruḥ. Llan deg yal akud.
upgrade-dialog-start-primary-button = Snirem afran n yiniten
upgrade-dialog-start-secondary-button = Mačči tura

## Colorway screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-colorway-title = Fren initen-ik·im
# This is shown to users with a custom home page, so they can switch to default.
upgrade-dialog-colorway-home-checkbox = Uɣal ɣer usebter agejdan n Firefox s yiniten n usentel-inek
upgrade-dialog-colorway-primary-button = Sekles afran n yini
upgrade-dialog-colorway-secondary-button = Ḥrez asentel yezrin
upgrade-dialog-colorway-theme-tooltip =
    .title = Snirem isental imezwar
# $colorwayName (String) - Name of colorway, e.g., Abstract, Cheers
upgrade-dialog-colorway-colorway-tooltip =
    .title = Snirem afran-a n yiniten { $colorwayName }
upgrade-dialog-colorway-default-theme = Amezwer
# "Auto" is short for "Automatic"
upgrade-dialog-colorway-theme-auto = Awurman
    .title = Ḍfer asentel n unagraw n wammud i tqeffalin, umuqen d yisfuyla
upgrade-dialog-theme-light = Aceεlal
    .title = Seqdec asentel aceεlal i tqeffalin, umuɣen d yiwfuyla
upgrade-dialog-theme-dark = Aberkan
    .title = Seqdec asentel aberkan i tqeffalin, umuɣen d yisfuyla
upgrade-dialog-colorway-variation-soft = Leqqaq
    .title = Seqdec afran-a n yiniten
upgrade-dialog-colorway-variation-balanced = Mnekni
    .title = Seqdec afran-a n yiniten
# "Bold" is used in the sense of bravery or courage, not in the sense of
# emphasized text.
upgrade-dialog-colorway-variation-bold = Azuran
    .title = Seqdec afran-a n yiniten

## Thank you screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-thankyou-title = Tanemmirt imi d nekkni i tferneḍ
upgrade-dialog-thankyou-subtitle = { -brand-short-name } d iminig ilelli i teḥrez tkebbanit ur nettnadi ɣef tedrimt. Akk akken, ad nerr web d aɣellsan, d azedgan yerna d udlig ugar.
upgrade-dialog-thankyou-primary-button = Bdu tunigin
